/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "main.h"

EFI_STATUS efi_main(EFI_HANDLE, EFI_SYSTEM_TABLE *);

EFI_STATUS efi_main(EFI_HANDLE image, EFI_SYSTEM_TABLE *sys)
{
	InitializeLib(image, sys);

	EFI_LOADED_IMAGE *loaded = NULL;
	EFI_STATUS status = uefi_call_wrapper(sys->BootServices->HandleProtocol, 3, image, &LoadedImageProtocol, (void **)&loaded);
	if(EFI_ERROR(status)) {
		Print(u"[boot]: unable to get loaded_image: %r\n", status);
	}

	Print(u"[boot]: Image base: 0x%lx\n", loaded->ImageBase);
	Print(u"[boot]: disabling watchdog for now\n");
	uefi_call_wrapper(sys->BootServices->SetWatchdogTimer, 4, 0, 0, 0, NULL);
	Print(u"[boot]: handing off to C++\n");
	return run(image, sys);
}
