/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_MEMORY_PAGER_INC
#	define ENTROPY_EOS_MEMORY_PAGER_INC

#	include "Descriptor.hh"
#	include "PageTableEntry.hh"

	namespace Entropy
	{
		namespace Eos
		{
			namespace Memory
			{
				// 2018-03-04 AMR TODO: allow requesting contigous pages
				class Pager
				{
					public:
						Pager();
						virtual ~Pager();
						virtual void *KPage();
						virtual void KFreePage(void *);
						virtual void AddMemory(Descriptor *);
					private:
						void _sort(Descriptor *);
						void _merge(Descriptor *);
					private:
						struct _mem_t {
							std::uintptr_t start;
							std::size_t count;
							std::size_t free;
							unsigned char *which;
							_mem_t *next;
						};
					private:
						_mem_t *_find_free();
						_mem_t *_find(void *);
						_mem_t *_next();
						void _add(const std::uintptr_t, const std::size_t);
					private:
						_mem_t *_start;
						std::size_t _page_end;
						bool _initialized;
						static constexpr std::uint8_t Slot[] = {
							1 << 0,
							1 << 1,
							1 << 2,
							1 << 3,
							1 << 4,
							1 << 5,
							1 << 6,
							1 << 7
						};
				};
			}
		}
	}

#endif
