/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_X86_GDT_IMPL
#	define ENTROPY_EOS_X86_GDT_IMPL

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				static inline void lgdt(RealPageTableEntry *where, const std::uint16_t size)
				{
					struct {
						std::uint16_t length;
						RealPageTableEntry *base;
					} __attribute__((__packed__)) val = {size, where};

					asm ("lidt %0" : : "m"(val));
				}
			}
		}
	}

#endif
