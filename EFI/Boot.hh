/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_EFI_BOOT_INC
#	define ENTROPY_EOS_EFI_BOOT_INC

#	ifndef ENTROPY_EOS_EFI_BUFFER_AMOUNT
#		define ENTROPY_EOS_EFI_BUFFER_AMOUNT 5
#	endif

#	include "../kernel/Boot.hh"
#	include "main.h"

	namespace Entropy
	{
		namespace Eos
		{
			namespace EFI
			{
				class Boot :
					public Eos::Boot
				{
					public:
						Boot(EFI_HANDLE, EFI_SYSTEM_TABLE *);
						~Boot();
						Memory::Descriptor *Free();
						Memory::Descriptor *Mine();
						Memory::Descriptor *Acpi();
						System *Exit();
						void PrintS(const char *);
						void PrintS(const std::size_t);
						void GetMap();
						void PrintMap();
					private:
						template<typename ...T>
						void _panic(const char16_t *, T...);
					private:
						void _fetch_efi_amt();
						void _fetch_efi_map();
						void _alloc_efi_map();
						void _alloc_krn_map();
						void _popul_krn_map();
						void _print_map();
						Memory::Descriptor *_alloc_desc(const std::size_t);
					private:
						EFI_HANDLE _image;
						EFI_SYSTEM_TABLE *_sys;
						bool _exited;
						bool _print;
					private:
						EFI_MEMORY_DESCRIPTOR *_desc;
						UINTN _size;
						UINTN _desc_size;
						UINTN _map_key;
						UINT32 _version;
					private:
						Memory::Descriptor *_free;
						Memory::Descriptor *_mine;
						Memory::Descriptor *_acpi;
					private:
						static constexpr std::size_t _buffer_extra = ENTROPY_EOS_EFI_BUFFER_AMOUNT;
				};
			}
		}
	}

	const CHAR16 *operator "" _efi (const char16_t *, const size_t);

#	include "Boot.impl.hh"

#endif
