/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Pager.hh"
#include <print.hh>

using namespace Entropy::Eos::Memory;
using namespace std;

using Type = PageTableEntry::Type;
using Size = PageTableEntry::Size;

using Entropy::Eos::detail::_out;

constexpr uint8_t Pager::Slot[];

Pager::Pager()
	: _start(nullptr), _initialized(false)
{}

Pager::~Pager() = default;

// 2018-03-04 AMR TODO: support multiple pages
void *Pager::KPage()
{
	if(!_initialized) {
		panic("[pager]: asking for memory before given any");
	}

	print("[pager]: KPage\n");
	uintptr_t where = 0;
	_mem_t *free = _find_free();

	for(auto x = 0u; x < free->count && where == 0; x++) {
		for(auto y = 0; y < 8 && where == 0; y++) {
			if(!(free->which[x] & Slot[y])) {
				free->which[x] |= Slot[y];
				free->free -= 1;

				where = free->start + (x * 8 + y) * 4096;
			}
		}
	}

	return reinterpret_cast<void *>(where);
}

// 2018-03-04 AMR TODO: support multiple pages?
void Pager::KFreePage(void *where)
{
	if(!_initialized) {
		panic("[pager]: asking to free memory before given any");
	}

	print("[pager]: KFreePage\n");
	_mem_t *who = _find(where);

	uintptr_t ptr = reinterpret_cast<uintptr_t>(where);
	uintptr_t slot = (ptr - who->start) / 4096 / 8;
	uintptr_t offset = (ptr - who->start) / 4096 % 8;

	who->which[slot] |= ~Slot[offset];
}

void Pager::AddMemory(Descriptor *free)
{
	if(free == nullptr)		// 2017-04-19 AMR NOTE: adding nothing
		return;

	print("[pager]: AddMemory\n");
	_sort(free);
	_merge(free);

	if(!_initialized) {
		if(free->Size == 0) {
			panic("[pager]: trying to initialize into 0 pages");
		}

		_page_end = 4096;
		_add(reinterpret_cast<uintptr_t>(free->Location), free->Size);

		_initialized = true;

		++free;
	}

	while(free->Size) {
		_add(reinterpret_cast<uintptr_t>(free->Location), free->Size);

		++free;
	}
}

Pager::_mem_t *Pager::_find_free()
{
	for(_mem_t *free = _start; free->count != 0; free = free->next) {
		if(free->free > 0)
			return free;
	}

	panic("[pager]: OOM");
}

Pager::_mem_t *Pager::_find(void *where)
{
	uintptr_t ptr = reinterpret_cast<uintptr_t>(where);
	for(_mem_t *free = _start; free->count != 0; free = free->next) {
		if(ptr >= free->start && ptr < free->start + free->count * 4096) {
			return free;
		}
	}

	panic("[pager]: freeing memory not ours");
}

Pager::_mem_t *Pager::_next()
{
	_mem_t *ret = _start;
	while(ret->count != 0) {
		ret = ret->next;
	}

	return ret;
}

void Pager::_add(const uintptr_t start, size_t count)
{
	print("[pager]: Adding memory, ", count, " pages\n");

	_mem_t *next = nullptr;

	// 2017-09-03 AMR NOTE: split the descriptor, if over 4096 * 8
	if(count > 4096 * 8) {
		_add(start + 4096 * 8, count - 4096 * 8);
		count = 4096 * 8;
	}

	if(!_initialized) {
		next = _start = reinterpret_cast<_mem_t *>(start);
		_page_end -= sizeof(_mem_t);
	} else {
		next = _next();
	}

	next->start = start;
	next->count = count;
	next->free = count;

	if(_page_end < count / 8 + (count % 8 ? 1 : 0)) {
		unsigned char *p = reinterpret_cast<unsigned char *>(KPage());
		next->which = p;
		_page_end = 4096;
	} else {
		next->which = reinterpret_cast<unsigned char *>(next->start + sizeof(_mem_t));
	}

	// 2017-09-03 AMR NOTE: guarantee map is marked as free
	for(auto x = 0u; x < count / 8 + (count % 8 ? 1 : 0); x++) {
		next->which[x] = 0;
	}

	if(!_initialized) {
		next->which[0] = 1;
		next->free -= 1;
	}

	_page_end -= count / 8 + (count % 8 ? 1 : 0);

	switch(count % 8) {
		case 1: next->which[count / 8] |= Slot[1]; [[fallthrough]];
		case 2: next->which[count / 8] |= Slot[2]; [[fallthrough]];
		case 3: next->which[count / 8] |= Slot[3]; [[fallthrough]];
		case 4: next->which[count / 8] |= Slot[4]; [[fallthrough]];
		case 5:	next->which[count / 8] |= Slot[5]; [[fallthrough]];
		case 6:	next->which[count / 8] |= Slot[6]; [[fallthrough]];
		case 7:	next->which[count / 8] |= Slot[7];
		break;
		case 0:
		break;
		default:
		break;
	}

	if(_page_end < sizeof(_mem_t)) {
		_mem_t *n = reinterpret_cast<_mem_t *>(KPage());
		next->next = n;
		_page_end = 4096;
	} else {
		next->next = reinterpret_cast<_mem_t *>(next->which + count / 8 + (count % 8 ? 1 : 0));
	}

	_page_end -= sizeof(_mem_t);
	next->next->count = 0;
}

void Pager::_sort(Descriptor *d)
{
	Descriptor t;

	for(auto x = 0u; d[x + 1].Size != 0; x++) {
		if(d[x].Location > d[x + 1].Location) {
			t = d[x];			// 2017-09-03 AMR TODO: change to swap when available
			d[x] = d[x + 1];
			d[x + 1] = t;
		}
	}
}

void Pager::_merge(Descriptor *d)
{
	Descriptor *current = d;
	Descriptor *next = d + 1;

	while(next != nullptr) {
		if(reinterpret_cast<uintptr_t>(current->Location) + current->Size == reinterpret_cast<uintptr_t>(next->Location)) {
			current->Size += next->Size;
			++next;
		} else {
			++current;
			++next;
		}
	}
}
