libk_DIR = $(SRCDIR)libk

libcxxrt_DIR = $(SRCDIR)external/libcxxrt/src

libk_INCLUDES = -I$(libk_DIR) -I$(libk_DIR)/include -I$(BUILD)/$(libk_DIR)/include
libk_CFLAGS = $(libk_INCLUDES) $(CFLAGS) -fPIC
libk_CXXFLAGS = $(libk_INCLUDES) $(CXXFLAGS) -fPIC

libcxxrt_CFLAGS = -DLIBCXXRT_WEAK_LOCKS $(libk_CFLAGS) -Wno-pedantic -Wno-switch-default -Wno-missing-declarations
libcxxrt_CXXFLAGS = -DLIBCXXRT_WEAK_LOCKS $(libk_CXXFLAGS) -Wno-missing-declarations -Wno-unused-parameter -Wno-zero-as-null-pointer-constant -Wno-shift-negative-value -Wno-useless-cast -Wno-shadow -Wno-missing-field-initializers

# 2018-01-21 AMR HACK: libcxxrt doesn't initialize bases explicitly
libcxxrt_CXXFLAGS += -Wno-extra

libk_HH_SRCS = \
	$(libk_DIR)/include/cctype.hh \
	$(libk_DIR)/include/cstdarg.hh \
	$(libk_DIR)/include/cstddef.hh \
	$(libk_DIR)/include/cstdlib.hh \
	$(libk_DIR)/include/cstdint.hh \
	$(libk_DIR)/include/cstdio.hh \
	$(libk_DIR)/include/cstring.hh \
	$(libk_DIR)/include/exception.hh \
	$(libk_DIR)/include/new.hh
libk_CC_SRCS = \
	$(libk_DIR)/c++abi/unwind.cc \
	$(libk_DIR)/posix/dlfcn.cc \
	$(libk_DIR)/posix/pthread.cc \
	$(libk_DIR)/posix/sched.cc \
	$(libk_DIR)/stdc++/new.cc \
	$(libk_DIR)/stdc/assert.cc \
	$(libk_DIR)/stdc/ctype.cc \
	$(libk_DIR)/stdc/errno.cc \
	$(libk_DIR)/stdc/stdio.cc \
	$(libk_DIR)/stdc/stdlib.cc \
	$(libk_DIR)/stdc/string.cc \
	$(libk_DIR)/print.cc

libcxxrt_C_SRCS = \
	$(libcxxrt_DIR)/cxa_atexit.c \
	$(libcxxrt_DIR)/cxa_finalize.c \
	$(libcxxrt_DIR)/libelftc_dem_gnu3.c
libcxxrt_CC_SRCS = \
	$(libcxxrt_DIR)/auxhelper.cc \
	$(libcxxrt_DIR)/dynamic_cast.cc \
	$(libcxxrt_DIR)/exception.cc \
	$(libcxxrt_DIR)/guard.cc \
	$(libcxxrt_DIR)/memory.cc \
	$(libcxxrt_DIR)/stdexcept.cc \
	$(libcxxrt_DIR)/terminate.cc \
	$(libcxxrt_DIR)/typeinfo.cc

libk_HH_OBJS = $(foreach src,$(libk_HH_SRCS),$(patsubst %.hh,$(BUILD)/%,$(src)))

libk_CC_OBJS = $(foreach src,$(libk_CC_SRCS),$(patsubst %,$(BUILD)/%.o,$(src)))
libk_CC_DEPS = $(foreach src,$(libk_CC_SRCS),$(patsubst %,$(BUILD)/%.dep,$(src)))

libk_OBJS = $(libk_CC_OBJS)
libk_DEPS = $(libk_CC_DEPS)

libcxxrt_C_OBJS = $(foreach src,$(libcxxrt_C_SRCS),$(patsubst %,$(BUILD)/%.o,$(src)))
libcxxrt_C_DEPS = $(foreach src,$(libcxxrt_C_SRCS),$(patsubst %,$(BUILD)/%.dep,$(src)))
libcxxrt_CC_OBJS = $(foreach src,$(libcxxrt_CC_SRCS),$(patsubst %,$(BUILD)/%.o,$(src)))
libcxxrt_CC_DEPS = $(foreach src,$(libcxxrt_CC_SRCS),$(patsubst %,$(BUILD)/%.dep,$(src)))

libcxxrt_OBJS = $(libcxxrt_C_OBJS) $(libcxxrt_CC_OBJS)
libcxxrt_DEPS = $(libcxxrt_C_DEPS) $(libcxxrt_CC_DEPS)

libk: $(libk_HH_OBJS) $(BUILD)/libk.a

$(BUILD)/libk.a: $(libk_OBJS) $(libcxxrt_OBJS)
	$(AR) rcs $@ $^

$(eval $(call HH-rule,$(libk_DIR)))
$(eval $(call CC-rule,$(libk_DIR),$(libk_CFLAGS),$(libk_DIR)))
$(eval $(call CXX-rule,$(libk_DIR),$(libk_CXXFLAGS),$(libk_DIR)))
$(eval $(call CC-rule,$(libcxxrt_DIR),$(libcxxrt_CFLAGS),$(libk_DIR)))
$(eval $(call CXX-rule,$(libcxxrt_DIR),$(libcxxrt_CXXFLAGS),$(libk_DIR)))

clean-libk:
	$(RM) $(wildcard $(libk_OBJS) $(libcxxrt_OBJS) $(BUILD)/libk.a)

.PHONY: libk clean-libk

-include $(libk_DEPS) $(libcxxrt_DEPS)
