/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_DLFCN_INC
#	define ENTROPY_EOS_LIBK_DLFCN_INC

#	ifdef __cplusplus
		extern "C" {
#	endif

			typedef struct {
				const char *dli_fname;
				void *dli_fbase;
				const char *dli_sname;
				void *dli_saddr;
			} Dl_info;

			int dladdr(void *, Dl_info *);

#	ifdef __cplusplus
		}
#	endif

#endif
