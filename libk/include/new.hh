/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_NEW_INC
#	define ENTROPY_EOS_LIBK_NEW_INC

#	include <cstddef>

	namespace std
	{
		class bad_alloc {};

		struct nothrow_t { explicit nothrow_t() = default; };
		extern const nothrow_t nothrow;
	}


	[[nodiscard]] void *operator new (std::size_t);
	[[nodiscard]] void *operator new (std::size_t, const std::nothrow_t &) noexcept;

	[[nodiscard]] void *operator new[] (std::size_t);
	[[nodiscard]] void *operator new[] (std::size_t, const std::nothrow_t &) noexcept;

	void operator delete (void *) noexcept;
	void operator delete (void *, const std::size_t) noexcept;
	void operator delete (void *, const std::nothrow_t &) noexcept;
	void operator delete (void *, const std::size_t, const std::nothrow_t &) noexcept;

	void operator delete[] (void *) noexcept;
	void operator delete[] (void *, const std::size_t) noexcept;
	void operator delete[] (void *, const std::nothrow_t &) noexcept;
	void operator delete[] (void *, const std::size_t, const std::nothrow_t &) noexcept;

#endif
