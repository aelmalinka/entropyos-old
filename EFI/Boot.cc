/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Boot.hh"
#include "System.hh"
#include <cstdint>

using namespace Entropy::Eos::EFI;
using namespace std;

using Entropy::Eos::Memory::Descriptor;

void *operator new[](size_t, void *);

void *operator new[](size_t, void *p)
{
	return p;
}

const CHAR16 *operator "" _efi (const char16_t *s, const size_t)
{
	return reinterpret_cast<const CHAR16 *>(s);
}

Boot::Boot(EFI_HANDLE image, EFI_SYSTEM_TABLE *sys) :
	_image(image),
	_sys(sys),
	_exited(false),
	_print(false),
	_desc(nullptr),
	_size(0),
	_desc_size(0),
	_map_key(0),
	_version(0),
	_free(nullptr),
	_mine(nullptr),
	_acpi(nullptr)
{
	Print(u"[uefi]: initializing\n"_efi);
}

Boot::~Boot()
{
	if(!_exited)
		Exit();
}

void Boot::PrintS(const char *s)
{
	while(*s)
		Print(u"%c"_efi, *s++);
}

void Boot::PrintS(const size_t v)
{
	Print(u"%ld"_efi, v);
}

Descriptor *Boot::Free()
{
	return _free;
}

Descriptor *Boot::Mine()
{
	return _mine;
}

Descriptor *Boot::Acpi()
{
	return _acpi;
}

Entropy::Eos::System *Boot::Exit()
{
	EFI_STATUS status = eos_efi_exit_boot_services(_sys, _image, _map_key);
	if(status == EFI_INVALID_PARAMETER)
		_panic(u"[uefi]: Memory map is outdated\n");
	else if(EFI_ERROR(status))
		_panic(u"[uefi]: ExitBootServices failed: %r\n", status);

	_exited = true;

	return new System(_sys);
}

void Boot::GetMap()
{
	Print(u"[uefi]: Getting memory map\n"_efi);

	_fetch_efi_amt();
	_alloc_efi_map();
	_fetch_efi_map();

	if(_print)
		_print_map();

	_alloc_krn_map();
	_fetch_efi_map();	// 2016-11-28 AMR NOTE: have to refetch map after allocating as it has changed
	_popul_krn_map();
}

void Boot::PrintMap()
{
	_print = true;
}

void Boot::_print_map()
{
	for(size_t x = 0; x < _size; x++) {
		EFI_MEMORY_DESCRIPTOR *d = reinterpret_cast<EFI_MEMORY_DESCRIPTOR *>(reinterpret_cast<uintptr_t>(_desc) + (x * _desc_size));
		if(d->NumberOfPages > 0)
		{
			if(
				d->Type == EfiConventionalMemory
			)
				Print(u"[uefi]: Usable	@ 0x%X (0x%X): %d\n"_efi, d->PhysicalStart, d->VirtualStart, d->NumberOfPages);
			else if(
				d->Type == EfiBootServicesCode ||
				d->Type == EfiBootServicesData
			)
				Print(u"[uefi]: EFI	@ 0x%X (0x%X): %d\n"_efi, d->PhysicalStart, d->VirtualStart, d->NumberOfPages);
			else if(
				d->Type == EfiLoaderCode
			)
				Print(u"[uefi]: Me	@ 0x%X (0x%X): %d\n"_efi, d->PhysicalStart, d->VirtualStart, d->NumberOfPages);
			else if(
				d->Type == EfiACPIReclaimMemory
			)
				Print(u"[uefi]: ACPI	@ 0x%X (0x%X): %d\n"_efi, d->PhysicalStart, d->VirtualStart, d->NumberOfPages);
			else if(
				d->Type == EfiLoaderData
			)
				Print(u"[uefi]: Self	@ 0x%X (0x%X): %d\n"_efi, d->PhysicalStart, d->VirtualStart, d->NumberOfPages);
			else
				Print(u"[uefi]: Used	@ 0x%X (0x%X): %d\n"_efi, d->PhysicalStart, d->VirtualStart, d->NumberOfPages);
		}
	}
}

void Boot::_fetch_efi_amt()
{
	UINTN sz = 0;

	EFI_STATUS status = eos_efi_get_memory_map(_sys, &sz, nullptr, &_map_key, &_desc_size, &_version);
	if(status != EFI_BUFFER_TOO_SMALL)
		_panic(u"[uefi]: GetMemoryMap amount, failed\n");

	_size = (sz / _desc_size);
	Print(u"[uefi]: memory map total entries %ld (size per: %ld, total size: %ld)\n"_efi, _size, _desc_size, sz);
}

void Boot::_alloc_efi_map()
{
	size_t sz = (_size + _buffer_extra) * _desc_size;
	EFI_STATUS status = eos_efi_allocate_pool(_sys, EfiLoaderData, sz, reinterpret_cast<void **>(&_desc));
	if(EFI_ERROR(status))
		_panic(u"[uefi]: Failed to allocate EFI Memory Descriptor table: %r\n", status);
}

void Boot::_fetch_efi_map()
{
	UINTN sz = (_size + _buffer_extra) * _desc_size;
	Print(u"[uefi]: fetching memory map\n"_efi);
	EFI_STATUS status = eos_efi_get_memory_map(_sys, &sz, _desc, &_map_key, &_desc_size, &_version);
	if(EFI_ERROR(status))
		_panic(u"[uefi]: Failed to get memory map: %r\n", status);
}

void Boot::_alloc_krn_map()
{
	size_t	free_sz = 0,
			mine_sz = 0,
			acpi_sz = 0
	;

	for(size_t x = 0; x < _size; x++)
	{
		EFI_MEMORY_DESCRIPTOR *d = reinterpret_cast<EFI_MEMORY_DESCRIPTOR *>(reinterpret_cast<uintptr_t>(_desc) + (x * _desc_size));
		if(d->NumberOfPages > 0)
		{
			switch(d->Type)
			{
				case EfiConventionalMemory:
					++free_sz;
				break;
				case EfiBootServicesCode:
				case EfiBootServicesData:
					++mine_sz;
				break;
				case EfiACPIReclaimMemory:
					++acpi_sz;
				break;
				default: break;
			}
		}
	}

	Print(u"[uefi]: Memory Free: %ld, Mine: %ld, ACPI: %ld, Total: %ld\n"_efi, free_sz, mine_sz, acpi_sz, _size);
	_free = _alloc_desc(free_sz);
	_mine = _alloc_desc(mine_sz);
	_acpi = _alloc_desc(acpi_sz);
}

void Boot::_popul_krn_map()
{
	size_t	free_loc = 0,
			mine_loc = 0,
			acpi_loc = 0;

	for(size_t x = 0; x < _size; x++)
	{
		EFI_MEMORY_DESCRIPTOR *d = reinterpret_cast<EFI_MEMORY_DESCRIPTOR *>(reinterpret_cast<uintptr_t>(_desc) + (x * _desc_size));
		if(d->NumberOfPages > 0)
		{
			switch(d->Type)
			{
				case EfiConventionalMemory:
					_free[free_loc].Location = reinterpret_cast<void *>(d->PhysicalStart);
					_free[free_loc++].Size = d->NumberOfPages;
				break;
				case EfiBootServicesCode:
				case EfiBootServicesData:
					_mine[mine_loc].Location = reinterpret_cast<void *>(d->PhysicalStart);
					_mine[mine_loc++].Size = d->NumberOfPages;
				break;
				case EfiACPIReclaimMemory:
					_acpi[acpi_loc].Location = reinterpret_cast<void *>(d->PhysicalStart);
					_acpi[acpi_loc++].Size = d->NumberOfPages;
				break;
				default: break;
			}
		}
	}

	_free[free_loc].Location = nullptr;
	_free[free_loc].Size = 0;

	_mine[mine_loc].Location = nullptr;
	_mine[mine_loc].Size = 0;

	_acpi[acpi_loc].Location = nullptr;
	_acpi[acpi_loc].Size = 0;
}

Descriptor *Boot::_alloc_desc(const size_t sz)
{
	void *where = nullptr;
	size_t amt = (sz + _buffer_extra) * _desc_size;
	EFI_STATUS status = eos_efi_allocate_pool(_sys, EfiLoaderData, amt, &where);
	if(EFI_ERROR(status) || where == nullptr)
		_panic(u"[uefi]: Failed to allocate space for Memory::Descriptor: %r\n", status);
	Memory::Descriptor *ret = new (where) Memory::Descriptor[sz + _buffer_extra];
	return ret;
}
