/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_MEMORY_PAGETABLEENTRY_INC
#	define ENTROPY_EOS_MEMORY_PAGETABLEENTRY_INC

// 2015-12-01 AMR TODO: detect page size options
#	ifndef SMALLEST_PAGE_SIZE
#		define SMALLEST_PAGE_SIZE 4096
#	endif

#	include <cstdint>

	namespace Entropy
	{
		namespace Eos
		{
			namespace Memory
			{
				// 2017-04-19 AMR TODO: allow w/ and w/o PAE, or just w/o PAE
				// 2017-04-19 AMR TODO: CR3, where are you
				// 2017-04-19 AMR TODO: maybe implement in C using packed union/struct
				class PageTableEntry
				{
					public:
						// 2015-12-01 AMR TODO: different architectures
						enum class Size
						{
							FourKb,
							TwoMb,
							OneGb
						};
						// 2015-12-01 AMR TODO: different architectures
						enum class Type
						{
							PageMapLevel4,
							PageDirectoryPointer,
							PageDirectory,
							PageTable
						};
						PageTableEntry(void *, const Size, const Type, bool Writable = true, bool User = false, bool Executable = false, bool Writethrough = false, bool CacheDisabled = false, bool Global = false);
						PageTableEntry(const std::uint64_t value, const Size, const Type);
						~PageTableEntry();
						const std::uint64_t &Value() const;
						const void *Where() const;
						const Size &size() const;
						const Type &type() const;
						bool Writable() const;
						bool User() const;
						bool Executable() const;
						bool Writethrough() const;
						bool CacheDisabled() const;
						bool Global() const;
					private:
						std::uint64_t _value;
						Size _size;
						Type _type;
				};

			}
		}
	}

#endif
