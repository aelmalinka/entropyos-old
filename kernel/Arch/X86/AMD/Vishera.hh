/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_KERNEL_ARCH_X86_AMD_VISHERA_INC
#	define ENTROPY_EOS_KERNEL_ARCH_X86_AMD_VISHERA_INC

#	include "../CPU.hh"

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				namespace AMD
				{
					class Vishera :
						public CPU
					{
						public:
							Vishera();
							virtual ~Vishera();
					};
				}
			}
		}
	}

#endif
