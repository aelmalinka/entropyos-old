/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_X86_CPU_INC
#	define ENTROPY_EOS_X86_CPU_INC

#	include <cstdint>

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				class CPU
				{
					public:
						CPU();
						virtual ~CPU();
						const char * Name() const;
					public:
						std::uint8_t Family() const;
						std::uint8_t Model() const;
					public:
						bool hasFPU() const;
						bool hasMSR() const;
						bool hasPAE() const;
						bool isApicEnabled() const;
						bool hasSysEnterExit() const;
						bool hasHTT() const;
					private:
						const char *_name;
						std::uint32_t _features[4];
					public:
						static CPU *Construct();
				};
			}
		}
	}

#endif
