/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Serial.hh"

#include "../Ports.hh"
#include <print.hh>
#include <cstddef>

using namespace std;
using namespace Entropy::Eos::X86;
using namespace Entropy::Eos::X86::Driver;

char *itoa(intmax_t, char *, const uintmax_t);

Serial::Serial()
	: Serial(_def_port)
{}

Serial::Serial(const uint16_t where)
	: _port(where)
{
	_test();
	_init();
}

Serial::~Serial()
{}

void Serial::_write(const char *p)
{
	while(*p)
		_write_char(*p++);
}

void Serial::_write(intmax_t v)
{
	// 2018-03-01 AMR TODO: dynamically allocate?
	char str[255];
	_write(itoa(v, str, 10));
}

void Serial::_write(const void *ptr)
{
	// 2018-03-01 AMR TODO: dynamically allocate?
	char str[255];
	_write("0x");
	_write(itoa(reinterpret_cast<uintptr_t>(ptr), str, 16));
}

// 2017-09-01 AMR TODO: should this be split off to a utility library?
char *itoa(intmax_t v, char *str, const uintmax_t base)
{
	size_t x = 0;

	if(v == 0) {
		str[x++] = '0';
		str[x] = 0;

		return str;
	}  else if (v < 0) {
		str[x++] = '-';
		v *= -1;
	}

	while(v != 0) {
		auto rem = v % base;
		str[x++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
		v /= base;
	}

	str[x] = 0;

	size_t i = 0;
	size_t j = x - 1;
	while(i < j) {
		char pivot = str[i];
		str[i++] = str[j];
		str[j--] = pivot;
	}

	return str;
}

void Serial::_init()
{
	writeb(_port + _intr_enable, 0x00);		// 2017-04-17 AMR NOTE: disable interupts for now
	writeb(_port + _line_control, 0x80);	// 2017-04-17 AMR NOTE: enable DLAB
	writeb(_port + _dlab_lo, 0x03);			// 2017-04-17 AMR NOTE: DLAB: 3 (lo byte 0x03, hi byte 0x00), 38400 baud
	writeb(_port + _dlab_hi, 0x00);
	writeb(_port + _line_control, 0x03);	// 2017-04-17 AMR NOTE: 8N1
	writeb(_port + _fifo, 0x07);			// 2017-04-17 AMR NOTE: 1 byte interrupt, clear in/out FIFO
	writeb(_port + _modem_control, 0x00);	// 2017-04-17 AMR NOTE: 2017-04-17 AMR TODO: figure this out
	writeb(_port + _scratch, 0x00);
}

void Serial::_test()
{
	if(readb(_port + _scratch) != 0x00) {
		panic("[serial]: not a serial port");
	}

	writeb(_port + _scratch, 0xDA);
	if(readb(_port + _scratch) != 0xDA) {
		panic("[serial]: not a serial port");
	}

	_init();

	// 2018-03-01 AMR TODO: what is this for?
	writeb(_port + _modem_control, 0x10);

	// 2017-04-17 AMR TODO: read stalls
	/*_write(0xFF);
	if(_read() == 0xFF)
		throw "Nope";

	_write(0xDA);
	if(_read() != 0xDA)
		throw "Nope";

	_write(0x00);
	if(_read() != 0x00)
		throw "Nope";*/
}

// 2017-09-01 AMR TODO: document binary offsets better
bool Serial::_data_waiting()
{
	return readb(_port + _line_status) & 0x01;
}

bool Serial::_transmit_ready()
{
	return readb(_port + _line_status) & 0x20;
}

uint8_t Serial::_read()
{
	while(!_data_waiting());
	return readb(_port);
}

void Serial::_write_char(const uint8_t v)
{
	while(!_transmit_ready());
	writeb(_port, v);
}
