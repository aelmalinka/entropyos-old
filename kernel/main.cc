/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "main.hh"
#include "Memory/new.hh"

#include <new>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <exception>

using namespace Entropy::Eos;
using namespace std;

Kernel::Kernel(Boot *boot)
	: _system(nullptr), _pager(), _allocator(_pager), _out(nullptr), _cpu()
{
#	ifdef DEBUG
		boot->PrintMap();
#	endif

	boot->GetMap();

	Memory::Descriptor
		*free = boot->Free(),
		*mine = boot->Mine(),
		*acpi = boot->Acpi()
	;

	// 2018-03-04 AMR NOTE: temporary output for pager/allocator debugging
	X86::Driver::Serial t;
	detail::_out = &t;

	(void)acpi;
	(void)mine;

	_pager.AddMemory(free);
	Memory::_alloc = &_allocator;

	_out = new X86::Driver::Serial;
	detail::_out = _out;
	print("[serial]: outputting panic here\n");

	_cpu = X86::CPU::Construct();
	print("[kernel]: booting on ", _cpu->Name(), " (", _cpu->Family(), " ", _cpu->Model(), ")\n");

	print("[kernel]: with features ",
			(_cpu->hasFPU() ? "+" : "-"), "FPU,",
			(_cpu->hasMSR() ? "+" : "-"), "MSR,",
			(_cpu->hasPAE() ? "+" : "-"), "PAE,",
			(_cpu->isApicEnabled() ? "+" : "-"), "Apic,",
			(_cpu->hasSysEnterExit() ? "+" : "-"), "SysEnterSysExit,",
			(_cpu->hasHTT() ? "+" : "-"), "HTT\n"
	);

	print("[kernel]: closing boot handle\n");
	_system = boot->Exit();

	print("[kernel]: destructing boot object\n");
	boot->~Boot();

	// 2017-09-04 AMR TODO: sometimes reclaiming efi causes a general proection fault
	// 2018-03-04 AMR NOTE: might have something to do with APIC or maybe GDT
	//_pager.AddMemory(mine);
}

Kernel::~Kernel()
{}

void Kernel::operator () ()
{
	print("[kernel]: booted\n");
	while(true);
}
