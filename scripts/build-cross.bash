#!/bin/bash
# Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

TARGET="$(uname -m)-elf"

[[ ! "${WORK}" ]] && WORK="$(pwd)"
[[ ! "${BUILD}" ]] && BUILD="${WORK}/build"
CROSS="${BUILD}/tools"
DISTS="${CROSS}/dist"

[[ -d "${DISTS}" ]] || mkdir -p "${DISTS}"

JOBS=$(($(grep processor /proc/cpuinfo | cut -d: -f 2 | sort -n | tail -n1) + 1))
PATH=$(echo ${PATH} | sed -e "s,${CROSS}/bin:,,g")

binutils_opts=(
	--build="$(uname -m)-pc-linux-gnu"
	--host="$(uname -m)-pc-linux-gnu"
	--target="${TARGET}"
	--prefix="${CROSS}"
	--with-sysroot
	--disable-nls
	--enable-targets=i386-efi-pe,x86_64-phoenix-freebsd,${TARGET}
	--enable-64-bit-bfd
)

gcc_opts=(
	AR_FOR_TARGET=${TARGET}-ar
	LD_FOR_TARGET=${TARGET}-ld
	--build="$(uname -m)-pc-linux-gnu"
	--host="$(uname -m)-pc-linux-gnu"
	--target="${TARGET}"
	--prefix="${CROSS}"
	--enable-languages=c,c++
	--disable-plugin
	--disable-libstdcxx
	--without-headers
	--disable-nls
)

die() {
	echo "$@" 1>&2
	exit
}

edo() {
	echo "$@"
	"$@" || die "$1 failed"
}

emake() {
	edo make -j ${JOBS:-1} $@
}

shopt -s extglob

BINUTILS_EXT=.tar.xz
GCC_EXT=.tar.xz

BINUTILS="$(echo "${DISTS}/"binutils-*${BINUTILS_EXT})"
GCC="$(echo "${DISTS}/"gcc-*${GCC_EXT})"

[[ "$1" != "--force" ]] && [[ -x "${CROSS}/bin/${TARGET}-gcc" ]] && exit

[[ ! -d "${CROSS}" ]] && edo mkdir "${CROSS}"
[[ ! -d "${DISTS}" ]] && edo mkdir "${DISTS}"
[[ ! -d "${CROSS}/build" ]] && edo mkdir "${CROSS}/build"

[[ -d "${CROSS}/build/binutils" ]] && edo rm -rf "${CROSS}/build/binutils"
[[ -d "${CROSS}/build/gcc" ]] && edo rm -rf "${CROSS}/build/gcc"

edo mkdir "${CROSS}/build/binutils"
edo mkdir "${CROSS}/build/gcc"

[[ ! -f "${BINUTILS}" ]] && die "Please put binutils ${BINUTILS_EXT} dist in ${DISTS}"
[[ ! -f "${GCC}" ]] && die "Please put gcc ${GCC_EXT} dist in ${DISTS}"

[[ ! -d "${BINUTILS%${BINUTILS_EXT}}" ]] && edo tar -xJf "${BINUTILS}" -C "${DISTS}"
[[ ! -d "${GCC%${GCC_EXT}}" ]] && edo tar -xJf "${GCC}" -C "${DISTS}"

pushd "${GCC%${GCC_EXT}}"
# 2015-12-06 AMR NOTE: AR is incorrectly set in libcpp
edo sed -i -e '/AR = ar/cAR := \$(AR)' libcpp/Makefile.in
# 2015-12-06 AMR NOTE: crtbeginS.o and crtendS.o are not built by default on *-*-elf
edo sed -i -e '/^\*-\*-elf)/{n;s,crtend.o,crtend.o crtbeginS.o crtendS.o,}' libgcc/config.host
popd

pushd "${CROSS}/build/binutils"
edo "${BINUTILS%${BINUTILS_EXT}}"/configure ${binutils_opts[@]}
emake
emake install
popd

pushd "${CROSS}/build/gcc"
edo "${GCC%${GCC_EXT}}"/configure ${gcc_opts[@]}
emake all-gcc
emake all-target-libgcc
emake install-gcc
emake install-target-libgcc
popd

cat << EOF > "${CROSS}/env"
export TARGET=${TARGET}
export PATH="${CROSS}/bin:${PATH}"
export CC=${TARGET}-gcc
export CXX=${TARGET}-g++
export AS=${TARGET}-as
export LD=${TARGET}-ld
export AR=${TARGET}-ar
EOF
