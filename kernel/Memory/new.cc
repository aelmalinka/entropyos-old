/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "new.hh"
#include <print.hh>

using namespace std;
using namespace Entropy::Eos;
using namespace Entropy::Eos::Memory;

Allocator *Entropy::Eos::Memory::_alloc = nullptr;
bad_alloc Entropy::Eos::Memory::_bad_alloc;

void *operator new (size_t size)
{
	if(_alloc == nullptr)
		panic("[memory]: new: we don't have an allocator yet, operator new");

	void *ptr = _alloc->allocate(size);
	if(ptr == nullptr)
		throw _bad_alloc;

	return ptr;
}

void *operator new (size_t size, const nothrow_t &) noexcept
{
	if(_alloc == nullptr)
		panic("[memory]: new: we don't have an allocator yet, operator new (nothrow)");

	return _alloc->allocate(size);
}

void *operator new[] (size_t size)
{
	return operator new(size);
}

void *operator new[] (size_t size, const nothrow_t &t) noexcept
{
	return operator new(size, t);
}

void operator delete (void *ptr) noexcept
{
	if(_alloc == nullptr)
		panic("[memory]: delete: we don't have an allocator yet, operator delete");

	_alloc->deallocate(ptr);
}

void operator delete (void *ptr, const size_t) noexcept
{
	operator delete(ptr);
}

void operator delete (void *ptr, const nothrow_t &) noexcept
{
	operator delete(ptr);
}

void operator delete (void *ptr, const size_t, const nothrow_t &) noexcept
{
	operator delete(ptr);
}

void operator delete[] (void *ptr) noexcept
{
	operator delete(ptr);
}

void operator delete[] (void *ptr, const size_t) noexcept
{
	operator delete[](ptr);
}

void operator delete[] (void *ptr, const nothrow_t &) noexcept
{
	operator delete[](ptr);
}

void operator delete[] (void *ptr, const size_t, const nothrow_t &) noexcept
{
	operator delete[](ptr);
}
