/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_KERNEL_MAIN_INC
#	define ENTROPY_EOS_KERNEL_MAIN_INC

#	include <cxa.h>
#	include <print.hh>

#	include "Boot.hh"

#	include "Memory/Allocator.hh"

//	2017-04-15 AMR TODO: abstract
#	include "Arch/X86/CPU.hh"
#	include "Arch/X86/Driver/Serial.hh"

	namespace Entropy
	{
		namespace Eos
		{
			class Kernel
			{
				public:
					Kernel(Boot *);
					~Kernel();
					void operator () ();
				private:
					System *_system;
					Memory::Pager _pager;
					Memory::Allocator _allocator;
					Output *_out;
				// 2017-09-03 AMR TODO: abstract
					X86::CPU *_cpu;
			};
		}
	}


#endif
