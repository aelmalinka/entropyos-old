/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_MEMORY_EXCEPTION_INC
#	define ENTROPY_EOS_MEMORY_EXCEPTION_INC

#	include "Allocator.hh"
#	include <cstddef>
#	include <new>

	namespace Entropy
	{
		namespace Eos
		{
			namespace Memory
			{
				extern Allocator *_alloc;
				extern std::bad_alloc _bad_alloc;
			}
		}
	}

#endif
