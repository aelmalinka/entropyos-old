/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <iostream>
#include <Memory/Allocator.hh>

using namespace std;

namespace Entropy { namespace Eos { namespace Memory {
	void debug_memory(const char *s, const List<Allocator::Descriptor> &l)
	{
#		ifdef DEBUG
			cout << "[debug]: " << s << endl;
			for(auto &d : l) {
				cout
					<< "\t" << reinterpret_cast<void *>(d.where) << " "
					<< reinterpret_cast<void *>(d.page) << " "
					<< d.size
					<< (d.used ? " used" : " unused") << endl;
			}
#		else
			(void)s;
			(void)l;
#		endif
	}
}}}
