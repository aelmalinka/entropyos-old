/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Bobcat.hh"
#include <print.hh>

using namespace Entropy::Eos::X86;
using namespace std;

AMD::Bobcat::Bobcat()
{
	print("[cpu]: running on AMD Bobcat\n");
}

AMD::Bobcat::~Bobcat()
{}
