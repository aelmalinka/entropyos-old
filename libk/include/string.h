/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_STRING_INC
#	define ENTROPY_EOS_LIBK_STRING_INC

#	include <stddef.h>

#	ifdef __cplusplus
	extern "C" {
#	endif

		void *memcpy(void * __restrict, const void * __restrict, size_t);
		void *memset(void *, int, size_t);

		int memcmp(const void *, const void *, size_t);

		int strcmp(const char *, const char *);
		int strncmp(const char *, const char *, size_t);

		char *strstr(const char *, const char *);

		size_t strlen(const char *);

		char *strcpy(char *, const char *);
		char *strncpy(char *, const char *, size_t);
		char *strdup(const char *);

#	ifdef __cplusplus
	}
#	endif

#endif
