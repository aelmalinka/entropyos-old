/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Allocator.hh"
#include <print.hh>

using namespace Entropy::Eos::Memory;

namespace Entropy { namespace Eos { namespace Memory {
	void debug_memory(const char *s, const List<Allocator::Descriptor> &l)
	{
#		ifdef DEBUG
			using Entropy::Eos::detail::_out;

			if(_out == nullptr) return;

			_out->Write("[memory]: ", s, "\n");

			// 2017-04-17 AMR TODO: abililty to output pointers
			for(auto &d : l) {
				_out->Write("\t",
					reinterpret_cast<void *>(d.where), " ",
					reinterpret_cast<void *>(d.page), " ",
					d.size,
					(d.used ? " used" : " unused"),
					"\n"
				);
			}
#		else
			(void)s;
			(void)l;
#		endif
	}
}}}
