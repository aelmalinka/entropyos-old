/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Vishera.hh"
#include <print.hh>

using namespace Entropy::Eos::X86;
using namespace std;

AMD::Vishera::Vishera()
{
	print("[cpu]: running on AMD Vishera Piledriver\n");
}

AMD::Vishera::~Vishera()
{}
