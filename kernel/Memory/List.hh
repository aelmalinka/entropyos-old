/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_MEMORY_LIST_INC
#	define ENTROPY_EOS_MEMORY_LIST_INC

#	include <print.hh>

	namespace Entropy
	{
		namespace Eos
		{
			namespace Memory
			{
				// 2017-04-19 AMR TODO: move app logic to Allocator
				template<typename T>
				class List
				{
					private:
						struct _element
						{
							_element();
							T elem;
							_element *next;
							_element *prev;
						};
					public:
						class Iterator
						{
							public:
								Iterator();
								Iterator(_element *);
								Iterator &operator ++();
								Iterator &operator --();
								const Iterator &operator ++() const;
								const Iterator &operator --() const;
								bool operator == (const Iterator &) const;
								bool operator != (const Iterator &) const;
								T &operator * ();
								T *operator -> ();
								const T &operator * () const;
								const T *operator -> () const;
								_element *ptr();
							private:
								_element *_ptr;
						};
						// 2015-12-01 AMR TODO: this is claiming an entire page for for new elements
						class Allocator
						{
							public:
								Allocator(Pager &);
								_element *create();
								// 2015-12-01 AMR TODO: remove pages that are no longer needed
								void remove(Iterator);
							private:
								void new_page();
								void initialize();
							private:
								Pager &_pager;
								bool _initialized;
								_element *_first;
								_element *_last;
								constexpr static std::size_t _buffer_size = SMALLEST_PAGE_SIZE / sizeof(_element);
						};
					public:
						List(Pager &);
						~List();
						Iterator begin() const;
						Iterator end() const;
						Iterator create();
						Iterator create(Iterator); 
						void remove(Iterator);
						void initialize();
					private:
						Allocator alloc;
						bool _initialized;
						_element *_first;
						_element *_last;
				};
			}
		}
	}

#	include "List.impl.hh"

#endif
