/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_KERNEL_MEMORY_DESCRIPTION_INC
#	define ENTROPY_EOS_KERNEL_MEMORY_DESCRIPTION_INC

#	include <cstddef>

	namespace Entropy
	{
		namespace Eos
		{
			namespace Memory
			{
				struct Descriptor {
					void *Location;
					std::size_t Size;
				};

			}
		}
	}

#endif
