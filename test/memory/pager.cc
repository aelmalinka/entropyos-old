/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <Memory/Pager.hh>

using namespace std;
using namespace testing;
using namespace Entropy::Eos;

namespace {
	class Pager :
		public Test
	{
		public:
			Pager() = default;
			~Pager() = default;
		protected:
			Memory::Descriptor *AddPages(const size_t);
		private:
			vector<vector<unsigned char>> _memory;
	};

	TEST_F(Pager, Create) {
		Memory::Pager p;
	}

	TEST_F(Pager, Basic) {
		Memory::Pager p;
		Memory::Descriptor *d = AddPages(4);

		p.AddMemory(d);

		delete[] d;

		void *a = p.KPage();
		void *b = p.KPage();
		void *c = p.KPage();

		EXPECT_NE(a, nullptr);
		EXPECT_NE(b, nullptr);
		EXPECT_NE(c, nullptr);

		p.KFreePage(a);
		p.KFreePage(b);
		p.KFreePage(c);
	}

	TEST_F(Pager, TwoTables) {
		Memory::Pager p;
		Memory::Descriptor *d1 = AddPages(2);
		Memory::Descriptor *d2 = AddPages(2);

		p.AddMemory(d1);
		p.AddMemory(d2);

		delete[] d1;
		delete[] d2;

		void *a = p.KPage();
		void *b = p.KPage();
		void *c = p.KPage();

		EXPECT_NE(a, nullptr);
		EXPECT_NE(b, nullptr);
		EXPECT_NE(c, nullptr);

		p.KFreePage(a);
		p.KFreePage(b);
		p.KFreePage(c);
	}

	TEST_F(Pager, TooManyPages) {
		Memory::Pager p;
		Memory::Descriptor *d1 = AddPages(4);
		Memory::Descriptor *d2 = AddPages(4096 * 8 * 4);

		p.AddMemory(d1);
		p.AddMemory(d2);

		delete[] d1;
		delete[] d2;

		void *a = p.KPage();
		void *b = p.KPage();
		void *c = p.KPage();

		EXPECT_NE(a, nullptr);
		EXPECT_NE(b, nullptr);
		EXPECT_NE(c, nullptr);

		p.KFreePage(a);
		p.KFreePage(b);
		p.KFreePage(c);
	}

	Memory::Descriptor *Pager::AddPages(const size_t count)
	{
		Memory::Descriptor *ret = new Memory::Descriptor[2];
		_memory.emplace_back(4096 * count, '\0');

		ret->Location = _memory.back().data();
		ret->Size = count;

		ret[1].Location = nullptr;
		ret[1].Size = 0;

		return ret;
	}
}
