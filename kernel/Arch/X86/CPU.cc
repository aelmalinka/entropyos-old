/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "CPU.hh"

#include <cstring>
#include <print.hh>

#include "AMD/Vishera.hh"
#include "AMD/Bobcat.hh"

using namespace Entropy::Eos::X86;
using namespace std;

static inline void cpuid(uint32_t &, uint32_t &, uint32_t &, uint32_t &);
static inline const char * cpuname();

static inline uint8_t family(const uint32_t);
static inline uint8_t model(uint32_t);

CPU::CPU()
	: _name(nullptr)
{
	_features[0] = 1;
	_features[1] = 0;
	_features[2] = 0;
	_features[3] = 0;

	_name = cpuname();
	cpuid(_features[0], _features[1], _features[2], _features[3]);
}

CPU::~CPU()
{
	panic("[cpu]: being destroyed\n");
}

const char *CPU::Name() const
{
	return _name;
}

// 2017-04-14 AMR NOTE: Stepping EAX[0:3]

uint8_t CPU::Family() const
{
	return family(_features[0]);
}

uint8_t CPU::Model() const
{
	return model(_features[0]);
}

bool CPU::hasFPU() const
{
	// 2017-04-13 AMR NOTE: FPU: EDX[0]
	return _features[3] & 1 << 0;
}

bool CPU::hasMSR() const
{
	// 2017-04-13 AMR NOTE: MSR: EDX[5]
	return _features[3] & 1 << 5;
}

bool CPU::hasPAE() const
{
	// 2017-04-13 AMR NOTE: PAE: EDX[6]
	return _features[3] & 1 << 6;
}

bool CPU::isApicEnabled() const
{
	// 2017-04-13 AMR NOTE: APIC: EDX[9]
	return _features[3] & 1 << 9;
}

bool CPU::hasSysEnterExit() const
{
	// 2017-04-13 AMR NOTE: SysEnterSysExit: EDX[11]
	return _features[3] & 1 << 11;
}

bool CPU::hasHTT() const
{
	// 2017-04-13 AMR NOTE: HTT: EDX[28]
	return _features[3] & 1 << 28;
}

CPU *CPU::Construct()
{
	uint32_t a = 1,
		unused = 0;

	const char *name = cpuname();
	cpuid(a, unused, unused, unused);

	uint8_t f = family(a);
	uint8_t m = model(a);

	// 2017-09-04 AMR NOTE: this could probably be better
	if(strcmp(name, "AuthenticAMD") == 0) {
		switch(f) {
			case 0x06:	// 2017-09-04 AMR NOTE: qemu is using this for qemu{32,64}
				return new CPU;
			case 0x0F:	// 2017-09-04 AMR NOTE: K8, also kvm{32,64}
				return new CPU;
			case 0x10:	// 2017-09-04 AMR NOTE: K10
				return new CPU;
			case 0x14:	// 2017-09-04 AMR NOTE: Bobcat
				return new AMD::Bobcat;
			case 0x15:
				if(m > 0x60 && m < 0x6F) {			// 2017-09-04 AMR NOTE: Excavator 2nd Revision
					return new CPU;
				} else if(m > 0x70 && m < 0x7F) {	// 2017-09-04 AMR NOTE: Excavator
					return new CPU;
				} else if(m > 0x30 && m < 0x3F) {	// 2017-09-04 AMR NOTE: Steamroller
					return new CPU;
				} else if(m > 0x10 && m < 0x1F) {	// 2017-09-04 AMR NOTE: Piledriver
					return new CPU;
				} else if(m == 0x02) {				// 2017-09-04 AMR NOTE: Vishera Pildriver
					return new AMD::Vishera;
				} else if(m == 0x00 || m == 0x01) {	// 2017-09-04 AMR NOTE: Bulldozer
					return new CPU;
				} else {							// 2017-09-04 AMR NOTE: Unknown
					return new CPU;
				}
			case 0x16:	// 2017-09-04 AMR NOTE: Jaguar
				return new CPU;
			case 0x17:	// 2017-09-04 AMR NOTE: Zen
				return new CPU;
			default:
				return new CPU;
		}
	} else {
		return new CPU;
	}
}

static inline const char *cpuname()
{
	static char ret[17] = "ThisIsNotAPr\0\0\0\0";
	uint32_t *ptr = reinterpret_cast<uint32_t *>(ret);

	cpuid(ptr[3], ptr[0], ptr[2], ptr[1]);

	ptr[3] = 0;
	return ret;
}

static inline void cpuid(uint32_t &a, uint32_t &b, uint32_t &c, uint32_t &d)
{
	asm volatile ("cpuid" :
		"=a"(a), "=b"(b), "=c"(c), "=d"(d) :
		"a"(a)
	);
}

static inline uint8_t family(const uint32_t feature)
{
	// 2017-04-14 AMR NOTE: BaseFamily EAX[8:11]
	// 2017-04-14 AMR NOTE: ExtFamily EAX[20:27]
	return
		((feature & 0xF00) >> 8) +
		((feature & 0xFF00000) >> 20)
	;
}

static inline uint8_t model(const uint32_t feature)
{
	// 2017-04-14 AMR NOTE: BaseModel EAX[4:7]
	// 2017-04-14 AMR NOTE: ExtModel EAX[16:19]
	return
		((feature & 0xF0) >> 4) |
		((feature & 0xF0000) >> 12)
	;
}
