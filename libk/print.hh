/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_PRINT_INC
#	define ENTROPY_EOS_PRINT_INC

#	include <cstdint>

	namespace Entropy
	{
		namespace Eos
		{
			[[noreturn]] void panic(const char *);

			template<typename T, typename ...Args>
			void print(T, Args ...);

			void print();

			class Output
			{
				public:
					Output() = default;
					virtual ~Output() = default;
					template<typename T, typename ...Args>
					void Write(T, Args ...);
					void Write() {}
				protected:
					virtual void _write(const char *) = 0;
					virtual void _write(const std::intmax_t) = 0;
					virtual void _write(const void *) = 0;
			};

			namespace detail
			{
				extern Output *_out;

				void _print(const char *);
				void _print(const std::intmax_t);
				void _print(void *);
			}
		}
	}

#	include "print.impl.hh"

#endif
