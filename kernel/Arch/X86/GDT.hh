/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_X86_GDT_INC
#	define ENTROPY_EOS_X86_GDT_INC

#	include <cstddef>
#	include <cstdint>

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				union RealPageTableEntry {
					struct {
						std::uint16_t LimitLo;
						struct __attribute__((__packed__)) {
							std::uint16_t Lo;
							std::uint8_t Hi;
						} BaseLo;
						std::uint8_t Access;
						std::uint8_t LimitHiFlags;
						std::uint8_t BaseHi;
					} Items;
					std::uint64_t Value;
				};

				static_assert(sizeof(RealPageTableEntry::Items) == 8);
				static_assert(sizeof(RealPageTableEntry) == 8);

				class PageTableEntry
				{
					public:
						PageTableEntry(RealPageTableEntry *);
						void setBase(const std::uint32_t);
						void setLimit(const std::uint32_t);
						void setPresent(const bool);
						void setPrivledge(const std::uint8_t);
						void setExecutable(const bool);
						void setDirectionConforming(const bool);
						void setReadWrite(const bool);
						void setGranularity(const bool);
						void setSize(const bool);
						void setX86_64(const bool);
						std::uint32_t Base() const;
						std::uint32_t Limit() const;
						bool Present() const;
						std::uint8_t Privledge() const;
						bool Executable() const;
						bool DirectionConforming() const;
						bool ReadWrite() const;
						bool Granularity() const;
						bool Size() const;
						bool X86_64() const;
						void Initialize();
					public:
						void setPtr(RealPageTableEntry *);
					private:
						RealPageTableEntry *_ptr;
					private:
						static constexpr std::uint8_t _present_bit = 1 << 7;
						static constexpr std::uint8_t _privledge_bits = 3 << 5;
						static constexpr std::uint8_t _system_bit = 1 << 4;
						static constexpr std::uint8_t _ex_bit = 1 << 3;
						static constexpr std::uint8_t _dc_bit = 1 << 2;
						static constexpr std::uint8_t _rw_bit = 1 << 1;
						static constexpr std::uint8_t _access_bit = 1;
						static constexpr std::uint8_t _gr_bit = 1 << 7;
						static constexpr std::uint8_t _sz_bit = 1 << 6;
						static constexpr std::uint8_t _x86_64_bit = 1 << 5;
						static constexpr std::uint8_t _limit_bits = 0xF;
				};

				class PageTableIterator
				{
					public:
						explicit PageTableIterator(RealPageTableEntry *);
						PageTableIterator(const PageTableIterator &);
						PageTableIterator &operator ++ ();
						PageTableIterator operator ++ (int);
						bool operator == (const PageTableIterator &);
						bool operator != (const PageTableIterator &);
						PageTableEntry &operator * ();
						PageTableEntry *operator -> ();
					private:
						RealPageTableEntry *_ptr;
						PageTableEntry _entry;
				};

				static inline void lgdt(RealPageTableEntry *, const std::uint16_t);

				class GDT
				{
					public:
						// 2018-03-04 AMR TODO: change size of gdt
						explicit GDT(const std::size_t = 0x10000);
						~GDT();
						PageTableIterator begin();
						PageTableIterator end();
						std::uint16_t size() const;
						PageTableEntry operator [] (const std::size_t);
					private:
						RealPageTableEntry *_start;
						std::size_t _sz;
						std::size_t _capacity;
					private:
						static RealPageTableEntry *Create(const std::size_t);
					public:
						static void Set(const GDT &);
				};
			}
		}
	}

#	include "GDT.impl.hh"

#endif
