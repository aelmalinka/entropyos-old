/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_SCHED_INC
#	define ENTROPY_EOS_LIBK_SCHED_INC

#	ifdef __cplusplus
		extern "C" {
#	endif

			int sched_yield();

#	ifdef __cplusplus
		}
#	endif

#endif
