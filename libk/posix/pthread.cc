/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "pthread.h"

pthread_once_t PTHREAD_ONCE_INIT;
pthread_mutex_t PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t PTHREAD_COND_INITIALIZER;
