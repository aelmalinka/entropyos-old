/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_KERNEL_X86_DRIVER_SERIAL_INC
#	define ENTROPY_EOS_KERNEL_X86_DRIVER_SERIAL_INC

#	include <print.hh>

#	ifndef ENTROPY_EOS_SERIAL_PORT
#		define ENTROPY_EOS_SERIAL_PORT 0x3F8
#	endif

#	ifndef ENTROPY_EOS_SERIAL_DATA_OFFSET
#		define ENTROPY_EOS_SERIAL_DATA_OFFSET 0
#	endif

#	ifndef ENTROPY_EOS_SERIAL_INTR_ENABLE_OFFSET
#		define ENTROPY_EOS_SERIAL_INTR_ENABLE_OFFSET 1
#	endif

#	ifndef ENTROPY_EOS_SERIAL_FIFO_OFFSET
#		define ENTROPY_EOS_SERIAL_FIFO_OFFSET 2
#	endif

#	ifndef ENTROPY_EOS_SERIAL_LINE_CONTROL_OFFSET
#		define ENTROPY_EOS_SERIAL_LINE_CONTROL_OFFSET 3
#	endif

#	ifndef ENTROPY_EOS_SERIAL_MODEM_CONTROL_OFFSET
#		define ENTROPY_EOS_SERIAL_MODEM_CONTROL_OFFSET 4
#	endif

#	ifndef ENTROPY_EOS_SERIAL_LINE_STATUS_OFFSET
#		define ENTROPY_EOS_SERIAL_LINE_STATUS_OFFSET 5
#	endif

#	ifndef ENTROPY_EOS_SERIAL_MODEM_STATUS_OFFSET
#		define ENTROPY_EOS_SERIAL_MODEM_STATUS_OFFSET 6
#	endif

#	ifndef ENTROPY_EOS_SERIAL_SCRATCH_OFFSET
#		define ENTROPY_EOS_SERIAL_SCRATCH_OFFSET 7
#	endif

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				namespace Driver
				{
					// 2018-03-01 AMR TODO: add interupt reading/writing support
					class Serial :
						public Eos::Output
					{
						public:
							Serial();
							Serial(const std::uint16_t);
							~Serial();
							void Write() {}
						private:
							void _write(const char *);
							void _write(std::intmax_t);
							void _write(const void *);
						private:
							void _init();
							void _test();
							bool _data_waiting();
							bool _transmit_ready();
							std::uint8_t _read();
							void _write_char(const std::uint8_t);
						private:
							std::uint16_t _port;
							static constexpr std::uint16_t _def_port = ENTROPY_EOS_SERIAL_PORT;
							static constexpr std::uint8_t _data = ENTROPY_EOS_SERIAL_DATA_OFFSET;
							static constexpr std::uint8_t _intr_enable = ENTROPY_EOS_SERIAL_INTR_ENABLE_OFFSET;
							static constexpr std::uint8_t _fifo = ENTROPY_EOS_SERIAL_FIFO_OFFSET;
							static constexpr std::uint8_t _line_control = ENTROPY_EOS_SERIAL_LINE_CONTROL_OFFSET;
							static constexpr std::uint8_t _modem_control = ENTROPY_EOS_SERIAL_MODEM_CONTROL_OFFSET;
							static constexpr std::uint8_t _line_status = ENTROPY_EOS_SERIAL_LINE_STATUS_OFFSET;
							static constexpr std::uint8_t _modem_status = ENTROPY_EOS_SERIAL_MODEM_STATUS_OFFSET;
							static constexpr std::uint8_t _scratch = ENTROPY_EOS_SERIAL_SCRATCH_OFFSET;
							static constexpr std::uint8_t _dlab_lo = _data;
							static constexpr std::uint8_t _dlab_hi = _intr_enable;
					};
				}
			}
		}
	}

#endif
