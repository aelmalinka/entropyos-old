/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_KERNEL_ARCH_X86_APIC_INC
#	define ENTROPY_EOS_KERNEL_ARCH_X86_APIC_INC

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				class Apic
				{
				};
			}
		}
	}

#endif
