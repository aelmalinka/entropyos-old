/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_CTYPE_INC
#	define ENTROPY_EOS_LIBK_CTYPE_INC

#	ifdef __cplusplus
	extern "C" {
#	endif

		int isalnum(int);
		int isalpha(int);
		int iscntrl(int);
		int isdigit(int);
		int isgraph(int);
		int islower(int);
		int isprint(int);
		int ispunct(int);
		int isspace(int);
		int isupper(int);
		int isxdigit(int);
		int isascii(int);
		int isblank(int);

		int tolower(int);
		int toupper(int);

#	ifdef __cplusplus
	}
#	endif

#endif
