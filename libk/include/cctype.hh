/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_CCTYPE_INC
#	define ENTROPY_EOS_LIBK_CCTYPE_INC

#	include <ctype.h>

#endif
