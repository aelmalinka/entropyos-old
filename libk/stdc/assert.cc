/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "assert.h"
#include "print.hh"

using namespace std;
using namespace Entropy::Eos;

extern "C" void __assert_fail(const char *cond, const char *file, const intmax_t line, const char *func)
{
	print("Assertion Failed: ", cond, ", function ", func, ", file ", file, ", line ", line);
	panic(".");
}
