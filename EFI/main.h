/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_EFI_MAIN_INC
#	define ENTROPY_EOS_EFI_MAIN_INC

#	ifdef __cplusplus
#		include <cstddef>
		extern "C" {
#	endif

#			ifdef DEBUG
#				undef DEBUG
#			endif

#			include <efi.h>
#			include <efilib.h>

			EFI_STATUS run(EFI_HANDLE, EFI_SYSTEM_TABLE *);
			EFI_STATUS eos_efi_exit_boot_services(EFI_SYSTEM_TABLE *, EFI_HANDLE, UINTN);
			EFI_STATUS eos_efi_get_memory_map(EFI_SYSTEM_TABLE *, UINTN *, EFI_MEMORY_DESCRIPTOR *, UINTN *, UINTN *, UINT32 *);
			EFI_STATUS eos_efi_allocate_pool(EFI_SYSTEM_TABLE *, EFI_MEMORY_TYPE, UINTN, void **);

#	ifdef __cplusplus
		}
#	endif

#endif
