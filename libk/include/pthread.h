/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_PTHREAD_INC
#	define ENTROPY_EOS_LIBK_PTHREAD_INC

#	include "sched.h"

#	ifdef __cplusplus
		extern "C" {
#	endif

			struct pthread_key_t {};
			struct pthread_once_t {};
			struct pthread_mutex_t {};
			struct pthread_mutexattr_t {};
			struct pthread_cond_t {};

			extern pthread_once_t PTHREAD_ONCE_INIT;
			extern pthread_mutex_t PTHREAD_MUTEX_INITIALIZER;
			extern pthread_cond_t PTHREAD_COND_INITIALIZER;

			int pthread_key_create(pthread_key_t *, void (*)(void *));
			int pthread_once(pthread_once_t *, void (*)(void));
			void *pthread_getspecific(pthread_key_t);
			int pthread_setspecific(pthread_key_t, const void *);
			int pthread_mutex_init(pthread_mutex_t *, const pthread_mutexattr_t *);
			int pthread_mutex_lock(pthread_mutex_t *);
			int pthread_mutex_unlock(pthread_mutex_t *);
			int pthread_cond_wait(pthread_cond_t *, pthread_mutex_t *);
			int pthread_cond_signal(pthread_cond_t *);

#	ifdef __cplusplus
		}
#	endif

#endif
