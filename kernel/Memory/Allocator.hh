/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_MEMORY_ALLOCATOR_INC
#	define ENTROPY_EOS_MEMORY_ALLOCATOR_INC

#	include <cstdint>
#	include "Pager.hh"
#	include "List.hh"

	namespace Entropy
	{
		namespace Eos
		{
			namespace Memory
			{
				// 2015-11-09 AMR TODO: data races
				// 2018-03-04 AMR TODO: support larger than 1 page alloc requests
				class Allocator
				{
					public:
						Allocator(Pager &);
						void *allocate(const std::size_t);
						void deallocate(void *);
						struct Descriptor
						{
							std::uintptr_t where;
							std::uintptr_t page;
							std::size_t size;
							bool used;
						};
					private:
						Pager &_pager;
						List<Descriptor> _memory;
						bool _initialized;
					private:
						void split(decltype(_memory)::Iterator, const std::size_t);
						void add_memory();
						void gc();
						decltype(_memory)::Iterator find(const std::size_t);
						decltype(_memory)::Iterator find(void *);
				};
				extern void debug_memory(const char *, const List<Allocator::Descriptor> &);
			}
		}
	}

#endif
