/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "GDT.hh"
#include <print.hh>
#include <new>

using namespace Entropy::Eos::X86;
using namespace Entropy::Eos;
using namespace std;

constexpr uint8_t PageTableEntry::_present_bit;
constexpr uint8_t PageTableEntry::_privledge_bits;
constexpr uint8_t PageTableEntry::_system_bit;
constexpr uint8_t PageTableEntry::_ex_bit;
constexpr uint8_t PageTableEntry::_dc_bit;
constexpr uint8_t PageTableEntry::_rw_bit;
constexpr uint8_t PageTableEntry::_access_bit;
constexpr uint8_t PageTableEntry::_gr_bit;
constexpr uint8_t PageTableEntry::_sz_bit;
constexpr uint8_t PageTableEntry::_x86_64_bit;
constexpr uint8_t PageTableEntry::_limit_bits;

RealPageTableEntry *GDT::Create(const size_t sz)
{
	print("[gdt]: allocating ",sz," entries of size ",sizeof(RealPageTableEntry),"\n");
	RealPageTableEntry *ret = new (nothrow) RealPageTableEntry[sz];

	if(ret == nullptr)
		panic("GDT::Create Failed to allocate memory");

	PageTableIterator end(ret + sz);

	for(PageTableIterator entry(ret); entry != end; entry++) {
		entry->Initialize();
	}

	return ret;
}

void GDT::Set(const GDT &gdt)
{
	lgdt(gdt._start, gdt._sz - 1);
}

void PageTableEntry::Initialize()
{
	_ptr->Value = 0;

	// 2018-03-04 AMR NOTE: for now always present
	setPresent(true);

	// 2018-03-04 AMR TODO: do we ever want to unset this bit?
	_ptr->Items.Access &= _system_bit;
}

void PageTableEntry::setLimit(const uint32_t v)
{
	if(v > 0xF'FFFF)
		panic("Limit can't be higher than 0xF'FFFF");

	_ptr->Items.LimitLo = v & 0xFFFF;
	_ptr->Items.LimitHiFlags = v & 0xF'0000;
}

void PageTableEntry::setBase(const uint32_t v)
{
	_ptr->Items.BaseLo.Lo = v & 0xFFFF;
	_ptr->Items.BaseLo.Hi = v & 0xFF'0000;
	_ptr->Items.BaseHi = v & 0xFF00'0000;
}

void PageTableEntry::setPresent(const bool v)
{
	if(v)
		_ptr->Items.Access |= _present_bit;
	else
		_ptr->Items.Access &= ~_present_bit;
}

void PageTableEntry::setPrivledge(const uint8_t v)
{
	if(v > 0b11)
		panic("Privledge can't be higher than 0b11");

	_ptr->Items.Access &= ~_privledge_bits;
	_ptr->Items.Access |= (v << 5);
}

void PageTableEntry::setExecutable(const bool v)
{
	if(v)
		_ptr->Items.Access |= _ex_bit;
	else
		_ptr->Items.Access &= ~_ex_bit;
}

void PageTableEntry::setDirectionConforming(const bool v)
{
	if(v)
		_ptr->Items.Access |= _dc_bit;
	else
		_ptr->Items.Access &= ~_dc_bit;
}

void PageTableEntry::setReadWrite(const bool v)
{
	if(v)
		_ptr->Items.Access |= _rw_bit;
	else
		_ptr->Items.Access &= ~_rw_bit;
}

void PageTableEntry::setGranularity(const bool v)
{
	if(v)
		_ptr->Items.LimitHiFlags |= _gr_bit;
	else
		_ptr->Items.LimitHiFlags &= ~_gr_bit;
}

void PageTableEntry::setSize(const bool v)
{
	if(v)
		_ptr->Items.LimitHiFlags |= _sz_bit;
	else
		_ptr->Items.LimitHiFlags &= ~_sz_bit;
}

void PageTableEntry::setX86_64(const bool v)
{
	if(v)
		_ptr->Items.LimitHiFlags |= _x86_64_bit;
	else
		_ptr->Items.LimitHiFlags &= ~_x86_64_bit;
}

uint32_t PageTableEntry::Limit() const
{
	// 2018-03-03 AMR TODO: is this the right bit order
	return
		_ptr->Items.LimitHiFlags & _limit_bits << 16 &
		_ptr->Items.LimitLo
	;
}

uint32_t PageTableEntry::Base() const
{
	// 2018-03-03 AMR TODO: is this the right bit order
	return
		_ptr->Items.BaseHi << 24 &
		_ptr->Items.BaseLo.Hi << 16 &
		_ptr->Items.BaseLo.Lo
	;
}

bool PageTableEntry::Present() const
{
	return _ptr->Items.Access & _present_bit;
}

uint8_t PageTableEntry::Privledge() const
{
	return _ptr->Items.Access & _privledge_bits;
}

bool PageTableEntry::Executable() const
{
	return _ptr->Items.Access & _ex_bit;
}

bool PageTableEntry::DirectionConforming() const
{
	return _ptr->Items.Access & _dc_bit;
}

bool PageTableEntry::ReadWrite() const
{
	return _ptr->Items.Access & _rw_bit;
}

bool PageTableEntry::Granularity() const
{
	return _ptr->Items.LimitHiFlags & _gr_bit;
}

bool PageTableEntry::Size() const
{
	return _ptr->Items.LimitHiFlags & _sz_bit;
}

bool PageTableEntry::X86_64() const
{
	return _ptr->Items.LimitHiFlags & _x86_64_bit;
}

GDT::GDT(const size_t sz)
	: _start(nullptr), _sz(sz)
{
	_start = Create(_sz);
}

GDT::~GDT()
{
	panic("[gdt]: being destroyed");
}

PageTableIterator GDT::begin()
{
	return PageTableIterator(_start);
}

PageTableIterator GDT::end()
{
	return PageTableIterator(_start + _sz);
}

PageTableEntry GDT::operator [] (const size_t v)
{
	return PageTableEntry(_start + v);
}

PageTableEntry::PageTableEntry(RealPageTableEntry *p)
	: _ptr(p)
{}

void PageTableEntry::setPtr(RealPageTableEntry *p)
{
	_ptr = p;
}

PageTableIterator::PageTableIterator(RealPageTableEntry *p) :
	_ptr(p),
	_entry(_ptr)
{}

PageTableIterator::PageTableIterator(const PageTableIterator &o) :
	_ptr(o._ptr),
	_entry(_ptr)
{}

PageTableIterator &PageTableIterator::operator ++ ()
{
	_entry.setPtr(++_ptr);

	return *this;
}

PageTableIterator PageTableIterator::operator ++ (int)
{
	PageTableIterator ret(*this);

	this->operator ++();

	return ret;
}

bool PageTableIterator::operator == (const PageTableIterator &o)
{
	return _ptr == o._ptr;
}

bool PageTableIterator::operator != (const PageTableIterator &o)
{
	return _ptr != o._ptr;
}

PageTableEntry &PageTableIterator::operator * ()
{
	return _entry;
}

PageTableEntry *PageTableIterator::operator -> ()
{
	return &_entry;
}
