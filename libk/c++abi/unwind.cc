/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "unwind.h"
#include <print.hh>
#include <cstdint>

using namespace std;
using namespace Entropy::Eos;

struct _Unwind_Context {
};

_Unwind_Reason_Code _Unwind_RaiseException(struct _Unwind_Exception *)
{
	print("[Unwind]: RaiseException to be implemented\n");
	return _URC_FATAL_PHASE1_ERROR;
}

void _Unwind_Resume(_Unwind_Exception *e)
{
	_Unwind_RaiseException(e);
	panic("[Unwind]: Resume trying to return\n");
}

// 2018-02-26 AMR TODO: Forced Unwind?
_Unwind_Reason_Code _Unwind_Resume_or_Rethrow(struct _Unwind_Exception *e)
{
	return _Unwind_RaiseException(e);
}

_Unwind_Reason_Code _Unwind_Backtrace(_Unwind_Trace_Fn, void *)
{
	print("[Unwind]: Backtrace to be implemenetd\n");
	return _URC_FATAL_PHASE1_ERROR;
}

void _Unwind_DeleteException(_Unwind_Exception *e)
{
	if(e && e->exception_cleanup)
		(*e->exception_cleanup)(_URC_FOREIGN_EXCEPTION_CAUGHT, e);
}

uintptr_t _Unwind_GetGR(_Unwind_Context *, int)
{
	print("[Unwind]: GetGR to be implemented\n");
	return 0;
}

uintptr_t _Unwind_GetIP(_Unwind_Context *)
{
	print("[Unwind]: GetIP to be implemented\n");
	return 0;
}

void *_Unwind_GetLanguageSpecificData(_Unwind_Context *)
{
	print("[Unwind]: GetLSDA to be implemented\n");
	return nullptr;
}

uintptr_t _Unwind_GetRegionStart(_Unwind_Context *)
{
	print("[Unwind]: GetRegionStart to be implemented\n");
	return 0;
}

uintptr_t _Unwind_GetDataRelBase(struct _Unwind_Context *)
{
	print("[Unwind]: GetDataRelBase to be implemented\n");
	return 0;
}

uintptr_t _Unwind_GetTextRelBase(struct _Unwind_Context *)
{
	print("[Unwind]: GetTextRelBase to be implemented\n");
	return 0;
}

void _Unwind_SetGR(_Unwind_Context *, int, uintptr_t)
{
	print("[Unwind]: SetGR to be implemented\n");
}

void _Unwind_SetIP(_Unwind_Context *, uintptr_t)
{
	print("[Unwind]: SetIP to be implemented\n");
}
