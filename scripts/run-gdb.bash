#!/bin/bash

if [[ $# -lt 1 ]] ; then
	echo "$0: ImageBase" 1>&2
	exit
fi

imagebase=$1
textoffset=$(objdump -wh -j .text build/kernel.efi | awk '{print $4}' | tail -n1 | tr '[:lower:]' '[:upper:]')
dataoffset=$(objdump -wh -j .data build/kernel.efi | awk '{print $4}' | tail -n1 | tr '[:lower:]' '[:upper:]')

textlocation=$(echo "obase=16;ibase=16;${imagebase}+${textoffset}" | bc)
datalocation=$(echo "obase=16;ibase=16;${imagebase}+${dataoffset}" | bc)

if [[ ! "${textlocation}" || ! "${datalocation}" ]] ; then
	echo "Failed to get locations" 1>&2
	exit
fi

exec gdb build/kernel.efi \
	-ex "file" \
	-ex "set architecture i386:x86-64:intel" \
	-ex "add-symbol-file build/kernel.sym 0x${textlocation} -s .data 0x${datalocation}" \
	-ex "target remote :1234"
