/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_PORTS_IMPL
#	define ENTROPY_EOS_PORTS_IMPL

#	include "Ports.hh"

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				static inline void writeb(const std::uint16_t where, const std::uint8_t what)
				{
					asm volatile ("outb %0, %1" : : "a"(what), "Nd"(where) );
				}

				static inline void writew(const std::uint16_t where, const std::uint16_t what)
				{
					asm volatile ("outw %0, %1" : : "a"(what), "Nd"(where) );
				}

				static inline void writel(const std::uint16_t where, const std::uint32_t what)
				{
					asm volatile ("outl %0, %1" : : "a"(what), "Nd"(where) );
				}

				static inline std::uint8_t readb(const std::uint16_t where)
				{
					std::uint8_t ret;
					asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(where));
					return ret;
				}

				static inline std::uint16_t readw(const std::uint16_t where)
				{
					std::uint16_t ret;
					asm volatile ("inw %1, %0" : "=a"(ret) : "Nd"(where));
					return ret;
				}

				static inline std::uint32_t readl(const std::uint16_t where)
				{
					std::uint32_t ret;
					asm volatile ("inl %1, %0" : "=a"(ret) : "Nd"(where));
					return ret;
				}
			}
		}
	}

#endif
