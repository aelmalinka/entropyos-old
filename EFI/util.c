/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "main.h"

EFI_STATUS eos_efi_exit_boot_services(EFI_SYSTEM_TABLE *sys, EFI_HANDLE image, UINTN map_key)
{
	return uefi_call_wrapper(sys->BootServices->ExitBootServices, 2, image, map_key);
}

EFI_STATUS eos_efi_get_memory_map(EFI_SYSTEM_TABLE *sys, UINTN *total_size, EFI_MEMORY_DESCRIPTOR *where, UINTN *map_key, UINTN *desc_size, UINT32 *version)
{
	return uefi_call_wrapper(sys->BootServices->GetMemoryMap, 5, total_size, where, map_key, desc_size, version);
}

EFI_STATUS eos_efi_allocate_pool(EFI_SYSTEM_TABLE *sys, EFI_MEMORY_TYPE type, UINTN amt, void **where)
{
	return uefi_call_wrapper(sys->BootServices->AllocatePool, 3, type, amt, where);
}
