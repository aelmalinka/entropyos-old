/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "string.h"
#include "stdlib.h"

void *memcpy(void * __restrict d, const void *__restrict s, size_t n)
{
	unsigned char *dst = const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(d));
	unsigned char *src = const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(s));

	for(auto x = 0u; x < n; x++) {
		dst[x] = src[x];
	}

	return d;
}

void *memset(void *d, int v, size_t n)
{
	unsigned char *dst = reinterpret_cast<unsigned char *>(d);

	for(auto x = 0u; x < n; x++) {
		dst[x] = v;
	}

	return d;
}

int memcmp(const void *p1, const void *p2, size_t n)
{
	unsigned char *a = const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(p1));
	unsigned char *b = const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(p2));

	while(n--) {
		if(*a != *b) {
			return *a - *b;
		}

		++a;
		++b;
	}

	return 0;
}

int strcmp(const char *a, const char *b)
{
	while(*a && *b && (*a == *b)) {
		++a; ++b;
	}

	return
		static_cast<unsigned char>(*a) -
		static_cast<unsigned char>(*b)
	;
}

int strncmp(const char *a, const char *b, size_t n)
{
	while(n && (*a == *b)) {
		++a; b++; --n;
	}

	if(n) {
		return
			static_cast<unsigned char>(*a) -
			static_cast<unsigned char>(*b)
		;
	} else {
		return 0;
	}
}

char *strstr(const char *s1, const char *s2)
{
	const char *p1 = s1;
	const char *p2;

	while(*s1) {
		p2 = s2;
		while(*p2 && (*p1 == *p2)) {
			++p1; ++p2;
		}
		if(!*p2) {
			return const_cast<char *>(s1);
		}
		++s1;
		p1 = s1;
	}

	return nullptr;
}

size_t strlen(const char *s)
{
	size_t ret = 0;

	while(s[ret++] != 0);

	return --ret;
}

char *strcpy(char *d, const char *s)
{
	for(auto x = 0u; x <= strlen(s); x++) {
		d[x] = s[x];
	}

	return d;
}

char *strncpy(char *d, const char *s, size_t n)
{
	while(n--) {
		d[n] = s[n];
	}

	return d;
}

char *strdup(const char *s)
{
	if(s) {
		char *ret = reinterpret_cast<char *>(malloc(strlen(s)));
		if(ret)
			strcpy(ret, s);
	}

	return nullptr;
}
