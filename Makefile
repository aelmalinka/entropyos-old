ARCH ?= $(shell uname -m)

TARGET = $(ARCH)-elf

SRCDIR ?=
BUILD ?= $(SRCDIR)build
CROSS := $(BUILD)/tools

PROJECTS := libk kernel EFI test
IMAGE := $(BUILD)/disk.img

default: $(PROJECTS) $(IMAGE)

CWARNINGS = -Wall -Wextra -Wpedantic \
	-Wshadow -Wcast-align -Wwrite-strings -Winline \
	-Wswitch-default -Wfloat-equal \
	-Wmissing-declarations \
	-Wtrampolines \
	-Werror
CXXWARNINGS = -Wall -Wextra -Wpedantic \
	-Wshadow -Wcast-align -Wwrite-strings -Winline \
	-Wswitch-default -Wfloat-equal \
	-Wmissing-declarations \
	-Wzero-as-null-pointer-constant -Wuseless-cast \
	-Wnoexcept -Wnon-virtual-dtor -Wold-style-cast -Woverloaded-virtual \
	-Wtrampolines \
	-Werror

CFLAGS ?= -g3 -Og
CFLAGS := $(CFLAGS) -std=c11 $(CWARNINGS)
CXXFLAGS ?= -g3 -Og
CXXFLAGS := $(CXXFLAGS) -std=c++17 $(CXXWARNINGS)

ifeq ($(DEBUG),1)
	CFLAGS += -DDEBUG
	CXXFLAGS += -DDEBUG
endif

HOSTCC ?= cc
HOSTCXX ?= c++

OBJCOPY ?= $(TARGET)-objcopy
STRIP ?= $(TARGET)-strip

DIR_GUARD=test -d $(@D) || mkdir -p $(@D)

define CC-rule
$$(BUILD)/$1/%.c.o: $1/%.c $3/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(CC) $2 -o $$@ -c $$<
$$(BUILD)/$1/%.c.dep: $1/%.c $3/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(CC) $2 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

define CXX-rule
$$(BUILD)/$1/%.cc.o: $1/%.cc $3/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(CXX) $2 -o $$@ -c $$<
$$(BUILD)/$1/%.cc.dep: $1/%.cc $3/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(CXX) $2 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

define HH-rule
$$(BUILD)/$1/%: $1/%.hh $1/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	echo -e "/* THIS IS A GENERATED FILE (Do Not Edit!)\n" \
		"Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>\n" \
		"Distributed under the terms of the GNU Affero General Public License v3\n" \
		"*/" \
		"#include \"$$(patsubst $1/%,%,$$<)\"\n" > $$@
endef

define HOSTCC-rule
$$(BUILD)/$1/%.host.c.o: $$(SRCDIR)$1/%.c $1/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(HOSTCC) $2 -o $$@ -c $$<
$$(BUILD)/$1/%.host.c.dep: $$(SRCDIR)$1/%.c $1/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(HOSTCC) $2 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

define HOSTCXX-rule
$$(BUILD)/$1/%.host.cc.o: $$(SRCDIR)$1/%.cc $1/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(HOSTCXX) $2 -o $$@ -c $$<
$$(BUILD)/$1/%.host.cc.dep: $$(SRCDIR)$1/%.cc $1/build.mk $$(SRCDIR)Makefile
	$$(DIR_GUARD)
	$$(HOSTCXX) $2 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

$(foreach module,$(PROJECTS),$(eval include $(SRCDIR)$(module)/build.mk))

clean: $(foreach module,$(PROJECTS),clean-$(module))
	$(RM) $(wildcard $(IMAGE) $(IMAGE)~ $(BUILD)/part.img $(BUILD)/part.img~)

depclean:
	find $(BUILD)/ -name '*.dep' | xargs $(RM)
	find $(BUILD)/ -type d -empty -delete

DISK_START=34
FAT_PADDING=512
DISK_PADDING=34

$(BUILD)/part.img: $(BUILD)/kernel.efi
	:; { \
		SECTORS=$$(echo "$$(du -B512 $(BUILD)/kernel.efi | awk '{print $$1+(n-$$1%n)%n}' n=32) + $(FAT_PADDING)" | bc) ; \
		dd if=/dev/zero of=$(BUILD)/part.img~ bs=512 count=$$SECTORS && \
		mkfs -t vfat $(BUILD)/part.img~ && \
		mmd -i $(BUILD)/part.img~ ::EFI && \
		mmd -i $(BUILD)/part.img~ ::EFI/Boot && \
		mcopy -si $(BUILD)/part.img~ $(BUILD)/kernel.efi ::EFI/Boot/BootX64.efi && \
		mv $(BUILD)/part.img~ $@ ; \
	}

$(IMAGE): $(BUILD)/part.img
	:; { \
		FAT_SECTORS=$$(du -B512 $(BUILD)/part.img | awk '{print $$1}') ; \
		SECTORS=$$(echo "$(DISK_START) + $(DISK_PADDING) + $$FAT_SECTORS" | bc) ; \
		dd if=/dev/zero of=$(IMAGE)~ bs=512 count=$$SECTORS && \
		echo -e "g\nn\n\n\n\nt\n1\nw\n" | fdisk $(IMAGE)~ && \
		dd if=$(BUILD)/part.img of=$(IMAGE)~ bs=512 obs=512 count=$$FAT_SECTORS seek=$(DISK_START) conv=notrunc && \
		mv $(IMAGE)~ $@ ; \
	}

.PHONY: default $(PROJECTS) clean depclean deps
