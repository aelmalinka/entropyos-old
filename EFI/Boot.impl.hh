/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_EFI_BOOT_IMPL
#	define ENTROPY_EOS_EFI_BOOT_IMPL

#	include "Boot.hh"

	namespace Entropy
	{
		namespace Eos
		{
			namespace EFI
			{
				template<typename ...T>
				void Boot::_panic(const char16_t *str, T ...v)
				{
					Print(reinterpret_cast<const CHAR16 *>(str), v...);
					while(true);
				}
			}
		}
	}

#endif
