/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_BOOT_INC
#	define ENTROPY_EOS_BOOT_INC

#	include "Memory/Descriptor.hh"
#	include "System.hh"

	namespace Entropy
	{
		namespace Eos
		{
			class Boot
			{
				public:
					virtual ~Boot() {}
					virtual Memory::Descriptor *Free() = 0;
					virtual Memory::Descriptor *Mine() = 0;
					virtual Memory::Descriptor *Acpi() = 0;
					virtual System *Exit() = 0;
					virtual void PrintS(const char *) = 0;
					virtual void PrintS(const std::size_t) = 0;
					virtual void GetMap() = 0;
					virtual void PrintMap() {}
			};
		}
	}

#endif
