/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <Memory/Allocator.hh>
#include <vector>
#include <list>

using namespace std;
using namespace testing;
using namespace Entropy::Eos;

namespace {
	class Allocator :
		public Test
	{
		public:
			Allocator() = default;
			~Allocator() = default;
		protected:
			Memory::Pager pager;
			void AddPages(const size_t);
		private:
			vector<vector<unsigned char>> _memory;
	};

	TEST_F(Allocator, Create) {
		Memory::Allocator a(pager);
	}

	TEST_F(Allocator, Basic) {
		Memory::Allocator a(pager);
		AddPages(3);	// 2017-09-03 AMR NOTE: need 1 page for pager, 1 page for allocator

		char *s = reinterpret_cast<char *>(a.allocate(sizeof(char) * strlen("Hello World!")));
		a.deallocate(s);
	}

	void Allocator::AddPages(const size_t count)
	{
		Memory::Descriptor *p = new Memory::Descriptor[2];
		_memory.emplace_back(4096 * count, '\0');

		p->Location = _memory.back().data();
		p->Size = count;

		p[1].Location = nullptr;
		p[1].Size = 0;

		pager.AddMemory(p);

		delete[] p;
	}
}
