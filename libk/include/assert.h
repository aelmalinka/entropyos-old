/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_ASSERT_INC
#	define ENTROPY_EOS_LIBK_ASSERT_INC

#	include <stdint.h>

#	ifdef __cplusplus
#		define __VOID_CAST(x) static_cast<void>(x)
#	else
#		define __VOID_CAST(x) ((void)(x))
#	endif

#	ifdef __cplusplus
	extern "C" {
#	endif

		void __assert_fail(const char *, const char *, const intmax_t, const char *);

#	ifdef __cplusplus
	}
#	endif

#	ifdef NDEBUG
#		define assert(ignore) (__VOID_CAST(0))
#	else
#		define assert(cond) ((cond) ? __VOID_CAST(0) : __assert_fail(#cond, __FILE__, __LINE__, __func__))
#	endif

#endif
