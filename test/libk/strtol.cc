/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include <cctype>

using namespace std;
using namespace testing;

namespace {
	TEST(strtol, Basic) {
		const char *s = "12345678901";

		EXPECT_EQ(strtol(s, nullptr, 10), 12345678901l);
		EXPECT_EQ(strtol(s, nullptr, 0), 12345678901l);
	}
}
