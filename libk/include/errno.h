/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_ERRNO_INC
#	define ENTROPY_EOS_LIBK_ERRNO_INC

#	ifdef __cplusplus
	extern "C" {
#	endif

		extern int *__eos_errno_func();
#		define errno (*__eos_errno_func())

#	ifdef __cplusplus
	}
#	endif

#endif
