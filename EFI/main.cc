/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "../kernel/main.hh"

#include "Boot.hh"
#include <exception>

#include "main.h"

using namespace std;
using namespace Entropy::Eos;

void *operator new(size_t, void *p);

void *operator new(size_t, void *p)
{
	return p;
}

extern "C" EFI_STATUS run(EFI_HANDLE image, EFI_SYSTEM_TABLE *sys)
{
	void *where = nullptr;

	Print(u"[boot]: allocating space for UEFI object, %ld bytes\n"_efi, sizeof(EFI::Boot));
	EFI_STATUS status = eos_efi_allocate_pool(sys, EfiLoaderData, sizeof(EFI::Boot), &where);
	if(EFI_ERROR(status))
	{
		Print(u"[boot]: Failed to allocate Efi Interface object: %r, 0x%lx\n"_efi, status, where);
		return status;
	}

	Print(u"[boot]: initializing UEFI object\n"_efi);
	EFI::Boot *boot = new (where) EFI::Boot(image, sys);

	try
	{
		Kernel k(boot);

		k();
	}
	catch(exception &e)
	{
		Print(u"[boot]: exception: %s\n"_efi, e.what());
		panic(e.what());
	}
	catch(...)
	{
		Print(u"[boot]: unknown exception in kernel object\n"_efi);
		panic("Unknown exception in kernel object");
	}

	return EFI_SUCCESS;
}
