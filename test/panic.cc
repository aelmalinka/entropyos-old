/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <cstdlib>
#include <iostream>
#include <gtest/gtest.h>
#include <print.hh>

using namespace std;
using namespace testing;
using namespace Entropy::Eos;

namespace {
	TEST(Panic, CanPanic) {
		EXPECT_DEATH_IF_SUPPORTED(panic("This is a death test"), "panic called: This is a death test");
	}
}

namespace Entropy { namespace Eos {
	[[noreturn]] void panic(const char *s)
	{
		cerr << "panic called: " << s << endl;
		exit(EXIT_FAILURE);
	}

	void print()
	{}

	namespace detail {
		void _print(const char *s)
		{
#			ifdef DEBUG
				clog << s << flush;
#			else
				(void)s;
#			endif
		}

		void _print(const intmax_t s)
		{
#			ifdef DEBUG
				clog << s << flush;
#			else
				(void)s;
#			endif
		}

		void _print(void *s)
		{
#			ifdef DEBUG
				clog << s << flush;
#			else
				(void)s;
#			endif
		}
	}
} }
