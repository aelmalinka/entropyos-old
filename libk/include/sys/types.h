/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_SYS_TYPES_INC
#	define ENTROPY_EOS_LIBK_SYS_TYPES_INC

	typedef int ssize_t;

#endif
