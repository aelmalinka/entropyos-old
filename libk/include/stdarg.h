/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_STDARG_INC
#	define ENTROPY_EOS_STDARG_INC

	typedef __builtin_va_list va_list;
#	define va_start(ap, last) (__builtin_va_start((ap), (last)))
#	define va_arg(ap, type) (__builtin_va_arg((ap), type))
#	define va_end(ap) (__builtin_va_end((ap)))
#	define va_copy(dest, src) (__builtin_va_copy((dest), (src)))

#endif
