/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_STDLIB_INC
#	define ENTROPY_EOS_LIBK_STDLIB_INC

#	include <stddef.h>

#	ifdef __cplusplus
	extern "C" {
#	endif

		void abort();

		long int strtol(const char * __restrict, char ** __restrict, int);

		void *malloc(size_t);
		void free(void *);
		void *calloc(size_t, size_t);
		void *realloc(void *, size_t);

#	ifdef __cplusplus
	}
#	endif

#endif
