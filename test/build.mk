test_DIR = $(srcdir)test

test_CFLAGS = $(CFLAGS)
# 2017-09-03 AMR NOTE: gtest currently uses NULL as of 1.8.0
test_CXXFLAGS = $(CXXFLAGS) -I$(BUILD)/tools/include -Wno-zero-as-null-pointer-constant -I$(kernel_DIR) -I$(test_DIR)
test_LDFLAGS = $(LDFLAGS) -L$(BUILD)/tools/lib -lpthread

test_LIBS = -lgtest -lgmock -lgmock_main

test_SRCS = \
	$(test_DIR)/libk/strtol.cc \
	$(test_DIR)/memory/debug.cc \
	$(test_DIR)/memory/allocator.cc \
	$(test_DIR)/memory/pager.cc \
	$(test_DIR)/panic.cc \
	$(kernel_DIR)/Memory/PageTableEntry.cc \
	$(kernel_DIR)/Memory/Pager.cc \
	$(kernel_DIR)/Memory/Allocator.cc \
	$(libk_DIR)/stdc/ctype.cc \
	$(libk_DIR)/stdc/stdlib.cc

test_OBJS = $(foreach src,$(test_SRCS),$(patsubst %.cc,$(BUILD)/%.host.cc.o,$(src)))
test_DEPS = $(foreach src,$(test_SRCS),$(patsubst %.cc,$(BUILD)/%.host.cc.dep,$(src)))

test: $(BUILD)/test/all
	$(BUILD)/test/all

check: test

clean-test:
	$(RM) $(wildcard $(test_OBJS) $(BUILD)/test/all)

$(BUILD)/test/all: $(test_OBJS)
	$(HOSTCXX) -o $@ $(test_LDFLAGS) $(test_LIBS) $^

$(eval $(call HOSTCXX-rule,$(test_DIR),$$(test_CXXFLAGS)))
$(eval $(call HOSTCXX-rule,$(libk_DIR),$$(test_CXXFLAGS)))
$(eval $(call HOSTCXX-rule,$(kernel_DIR),$$(test_CXXFLAGS)))

.PHONY: check test clean-test

-include $(test_DEPS)
