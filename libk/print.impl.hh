/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_KERNEL_PRINT_IMPL
#	define ENTROPY_EOS_KERNEL_PRINT_IMPL

#	include "print.hh"

	namespace Entropy
	{
		namespace Eos
		{
			template<typename T, typename ...Args>
			void print(T v, Args ...args)
			{
				detail::_print(v);
				print(args...);
			}

			template<typename T, typename ...Args>
			void Output::Write(T v, Args ...args)
			{
				_write(v);
				Write(args...);
			}
		}
	}

#endif
