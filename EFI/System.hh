/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_EFI_SYSTEM_INC
#	define ENTROPY_EOS_EFI_SYSTEM_INC

#	include "../kernel/System.hh"
#	include "main.h"

	namespace Entropy
	{
		namespace Eos
		{
			namespace EFI
			{
				class System :
					public Eos::System
				{
					public:
						System(EFI_SYSTEM_TABLE *);
						~System();
					private:
						EFI_SYSTEM_TABLE *_sys;
				};
			}
		}
	}

#endif
