/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "errno.h"

int _actual_error_number;

extern "C" int *__eos_errno_func()
{
	return &_actual_error_number;
}
