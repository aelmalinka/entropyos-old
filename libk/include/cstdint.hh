/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_CSTDINT_INC
#	define ENTROPY_EOS_LIBK_CSTDINT_INC

	// 2018-03-04 AMR TODO: should we include cstddef?

#	include <stdint.h>

	namespace std
	{
		using ::int8_t;
		using ::int16_t;
		using ::int32_t;
		using ::int64_t;

		using ::int_least8_t;
		using ::int_least16_t;
		using ::int_least32_t;
		using ::int_least64_t;

		using ::int_fast8_t;
		using ::int_fast16_t;
		using ::int_fast32_t;
		using ::int_fast64_t;

		using ::intmax_t;
		using ::intptr_t;

		using ::uint8_t;
		using ::uint16_t;
		using ::uint32_t;
		using ::uint64_t;

		using ::uint_least8_t;
		using ::uint_least16_t;
		using ::uint_least32_t;
		using ::uint_least64_t;

		using ::uint_fast8_t;
		using ::uint_fast16_t;
		using ::uint_fast32_t;
		using ::uint_fast64_t;

		using ::uintmax_t;
		using ::uintptr_t;
	}

#endif
