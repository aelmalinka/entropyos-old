/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_STDIO_INC
#	define ENTROPY_EOS_LIBK_STDIO_INC

#	include <stddef.h>
#	include <stdarg.h>

#	ifdef __cplusplus
	extern "C" {
#	endif

		typedef int FILE;

		extern FILE *stdin;
		extern FILE *stdout;
		extern FILE *stderr;

		size_t fread(void *, size_t, size_t, FILE *);
		size_t fwrite(const void *, size_t, size_t, FILE *);

		int printf(const char *, ...);
		int fprintf(FILE *, const char *, ...);
		int snprintf(char * __restrict, size_t, const char * __restrict, ...);
		int asprintf(char **, const char *, ...);

		int vprintf(const char *, va_list);
		int vfprintf(FILE *, const char *, va_list);
		int vsnprintf(char * __restrict, size_t, const char * __restrict, va_list);
		int vasprintf(char **, const char *, va_list);

#	ifdef __cplusplus
	}
#	endif

#endif
