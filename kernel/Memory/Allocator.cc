/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Allocator.hh"

using namespace Entropy::Eos::Memory;
using namespace std;

Allocator::Allocator(Pager &pager)
	: _pager(pager), _memory(pager), _initialized(false)
{}

void *Allocator::allocate(const size_t size)
{
	if(!_initialized) {
		_memory.initialize();
		_initialized = true;
	}

	auto i = _memory.begin();
	while((i = find(size)) ==  _memory.end())
		add_memory();

	if(size > SMALLEST_PAGE_SIZE)
	{
		auto j = i;
		size_t found = j->size;
		while((found += (++j)->size) < size)
		{
			j->used = true;
		}
		if(found > size)
			split(j, found - size);
		i->size = size;
	}
	else if(i->size > size)
		split(i, size);

	i->used = true;

	debug_memory("allocate", _memory);
	return reinterpret_cast<void *>(i->where);
}

void Allocator::deallocate(void *addr)
{
	if(!_initialized) {
		_memory.initialize();
		_initialized = true;
	}

	auto i = find(addr);

	if(i->size > SMALLEST_PAGE_SIZE)
	{
		size_t stop = i->size;
		i->size = SMALLEST_PAGE_SIZE;

		auto j = i;
		size_t found = j->size;
		while((found += (++j)->size) < stop)
			j->used = false;
	}

	i->used = false;

	debug_memory("deallocate", _memory);
	gc();
}

void Allocator::split(decltype(_memory)::Iterator i, const size_t size)
{
	if(size > SMALLEST_PAGE_SIZE)
		panic("Allocator spliting descriptor that's across page boundries");

	auto j = _memory.create(i);

	j->where = i->where + size;
	j->page = i->page;
	j->size = i->size - size;
	j->used = false;

	i->size = size;
	debug_memory("split", _memory);
}

void Allocator::add_memory()
{
	uintptr_t page = reinterpret_cast<uintptr_t>(_pager.KPage());
	auto i = _memory.create();

	i->where = page;
	i->page = page;
	i->size = SMALLEST_PAGE_SIZE;
	i->used = false;

	debug_memory("add_memory", _memory);
}

// 2015-12-01 AMR TODO: only clean when needed
void Allocator::gc()
{
	auto i = _memory.begin();
	auto j = _memory.begin();

	++j;

	while(i != _memory.end() && j != _memory.end())
	{
		if(
			i->where + i->size == j->where &&	// 2015-12-01 AMR NOTE: contigous
			i->page == j->page &&				// 2015-12-01 AMR NOTE: on the same page
			!i->used && !j->used				// 2015-12-01 AMR NOTE: not used
		)
		{
			i->size += j->size;

			auto k = j;
			++j;
			_memory.remove(k);
			--i; --j;
		}
		++i; ++j;
	}

	i = _memory.begin();
	j = _memory.begin();

	++j;

	while(i != _memory.end() && j != _memory.end())
	{
		if(!i->used && i->page != j->page)		// 2015-12-01 AMR NOTE: if the next in list is on different page this is only one for page because of above
		{
			auto page = i->page;
			auto k = i;
			++i; ++j;
			_memory.remove(k);
			_pager.KFreePage(reinterpret_cast<void *>(page));
			--i; --j;
		}
		++i; ++j;
	}
	debug_memory("gc", _memory);
}

decltype(Allocator::_memory)::Iterator Allocator::find(const size_t size)
{
	debug_memory("find size", _memory);
	auto i = _memory.begin();
	bool found = false;

	while(i != _memory.end() && !found)
	{
		if(!i->used && size <= SMALLEST_PAGE_SIZE && i->size >= size)
			found = true;
		else if(!i->used)
		{
			size_t amt = i->size;
			auto j = i;
			auto k = i;
			while(
				++i != _memory.end() &&
				j != _memory.end() &&
				!i->used &&
				j->where + j->size == i->where
			)
			{
				if((amt += i->size) >= size)
				{
					i = k;
					found = true;
					break;
				}
				++j;
			}
		}
		if(!found && i != _memory.end())
			++i;
	}

	return i;
}

decltype(Allocator::_memory)::Iterator Allocator::find(void *p)
{
	auto ptr = reinterpret_cast<uintptr_t>(p);
	for(auto i = _memory.begin(); i != _memory.end(); ++i)
	{
		if(i->where == ptr)
			return i;
	}

	panic("Failed to find Descriptor to remove");
	return _memory.end();
}
