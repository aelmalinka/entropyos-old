/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_EXCEPTION_INC
#	define ENTROPY_EOS_LIBK_EXCEPTION_INC

	namespace std
	{
		class exception
		{
			public:
				exception() noexcept;
				exception(const exception &) noexcept;
				virtual ~exception();
				exception &operator = (const exception &) noexcept;
				virtual const char *what() const noexcept;
		};
	}

#endif
