#!/bin/bash
# Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

[[ ! "${SRC}" ]] && WORK="$(pwd)"
[[ ! "${BUILD}" ]] && BUILD="${WORK}/build"
[[ ! "${GTEST}" ]] && GTEST="${WORK}/external/googletest"
[[ ! "${PREFIX}" ]] && PREFIX="${BUILD}/tools"

JOBS=$(($(grep processor /proc/cpuinfo | cut -d: -f 2 | sort -n | tail -n1) + 1))

[[ ! "${HOSTCC}" ]] && HOSTCC=`which cc`
[[ ! "${HOSTCXX}" ]] && HOSTCXX=`which c++`
[[ ! "${HOSTAR}" ]] && HOSTAR=`which ar`
[[ ! "${HOSTRANLIB}" ]] && HOSTRANLIB=`which ranlib`

die() {
	echo "$@" 1>&2
	exit
}

edo() {
	echo "$@"
	"$@" || die "$1 failed"
}

ecmake() {
	edo cmake \
		-DCMAKE_INSTALL_PREFIX:PATH="${PREFIX}" \
		-DCMAKE_VERBOSE_MAKEFILE:BOOLEAN=True \
		-DCMAKE_AR="${HOSTAR}" \
		-DCMAKE_RANLIB="${HOSTRANLIB}" \
		-DCMAKE_C_COMPILER="${HOSTCC}" \
		-DCMAKE_CXX_COMPILER="${HOSTCXX}" \
		$@ ${GTEST}
}

emake() {
	edo make -j${JOBS:-1} $@
}

[[ ! -d "${PREFIX}/build/gtest" ]] && edo mkdir -p "${PREFIX}/build/gtest"

pushd "${PREFIX}/build/gtest"
ecmake
emake
emake install
popd
