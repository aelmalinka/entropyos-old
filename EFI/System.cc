/*	Copyright 2016 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "System.hh"

using namespace Entropy::Eos::EFI;
using namespace std;

System::System(EFI_SYSTEM_TABLE *sys)
	: _sys(sys)
{}

System::~System()
{}
