/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_MEMORY_LIST_IMPL
#	define ENTROPY_EOS_MEMORY_LIST_IMPL

#	include "List.hh"

	namespace Entropy
	{
		namespace Eos
		{
			namespace Memory
			{
				template<typename T>
				void List<T>::Allocator::remove(Iterator i)
				{
					auto j = i.ptr();

					j->prev = _last;
					_last->next = j;
					
					_last = j;

					// 2017-04-19 AMR HACK: wtf, super specific and platform dependent
					_last->elem.page = reinterpret_cast<std::uintptr_t>(j) & 0xFFFF'FFFF'FFFF'F000;
				}

				template<typename T>
				void List<T>::remove(Iterator i)
				{
					i->used = true;
					i->size = 0;
					i->where = 0;
					i->page = 0;

					auto j = i;
					auto k = i;
					--j; ++k;
					j.ptr()->next = k.ptr();
					k.ptr()->prev = j.ptr();

					if(i.ptr() == _first)
						_first = k.ptr();

					alloc.remove(i);
				}

				template<typename T>
				typename List<T>::_element *List<T>::Allocator::create()
				{
					if(!_initialized) {
						initialize();
					}

					_first = _first->next;
					if(_first == _last)
						new_page();

					return _first->prev;
				}

				template<typename T>
				void List<T>::Allocator::new_page()
				{
					auto page = reinterpret_cast<std::uintptr_t>(_pager.KPage());
					auto first = reinterpret_cast<_element *>(page);

					for(std::size_t x = 0; x < _buffer_size; ++x)
					{
						auto curr = first + x;

						curr->prev = curr - 1;
						curr->next = curr + 1;
						curr->elem.page = page;
					}

					first->prev = _first;
					_first->next = first;

					_last = first + _buffer_size - 1;
				}

				template<typename T>
				void List<T>::Allocator::initialize()
				{
					auto page = reinterpret_cast<std::uintptr_t>(_pager.KPage());
					auto first = reinterpret_cast<_element *>(page);

					for(std::size_t x = 0; x < _buffer_size; ++x)
					{
						auto curr = first + x;

						curr->prev = curr - 1;
						curr->next = curr + 1;
						curr->elem.page = page;
					}

					_first = first;
					_last = first + _buffer_size - 1;
					_initialized = true;
				}

				template<typename T>
				List<T>::Allocator::Allocator(Pager &p)
					: _pager(p), _initialized(false)
				{}

				template<typename T>
				List<T>::List(Pager &p)
					: alloc(p), _initialized(false)
				{}

				template<typename T>
				List<T>::~List()
				{
					if(_initialized) {
						auto i = begin();
						auto j = i;
						while(i != end())
						{
							++i;
							remove(j);
							j = i;
						}
					}
				}

				template<typename T>
				void List<T>::initialize()
				{
					_last = alloc.create();
					_first = _last;

					_last->next = nullptr;
					_last->prev = nullptr;
					_initialized = true;
				}

				template<typename T>
				typename List<T>::Iterator List<T>::create()
				{
					if(!_initialized) {
						initialize();
					}

					auto i = alloc.create();

					if(_first == _last)
						_first = i;
					else
					{
						i->prev = _last->prev;
						_last->prev->next = i;
					}

					i->next = _last;
					_last->prev = i;

					return i;
				}

				// 2015-12-01 AMR TODO: try and combine these two
				template<typename T>
				typename List<T>::Iterator List<T>::create(Iterator i)
				{
					if(!_initialized) {
						initialize();
					}

					if(i == end())
						return create();

					auto j = alloc.create();
					j->prev = i.ptr();
					j->next = i.ptr()->next;
					i.ptr()->next = j;
					j->next->prev = j;

					return j;
				}

				template<typename T>
				typename List<T>::Iterator List<T>::begin() const
				{
					if(!_initialized) {
						panic("[List<T>]: unable to initialize");
					}

					return _first;
				}

				template<typename T>
				typename List<T>::Iterator List<T>::end() const
				{
					if(!_initialized) {
						panic("[List<T>]: unable to initialize");
					}

					return _last;
				}

				template<typename T>
				List<T>::Iterator::Iterator()
					: _ptr(nullptr)
				{}

				template<typename T>
				List<T>::Iterator::Iterator(_element *p)
					: _ptr(p)
				{}

				template<typename T>
				typename List<T>::Iterator &List<T>::Iterator::operator ++()
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					_ptr = _ptr->next;
					return *this;
				}

				template<typename T>
				typename List<T>::Iterator &List<T>::Iterator::operator --()
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					_ptr = _ptr->prev;
					return *this;
				}

				template<typename T>
				const typename List<T>::Iterator &List<T>::Iterator::operator ++() const
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					_ptr = _ptr->next;
					return *this;
				}

				template<typename T>
				const typename List<T>::Iterator &List<T>::Iterator::operator --() const
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					_ptr = _ptr->prev;
					return *this;
				}

				template<typename T>
				bool List<T>::Iterator::operator == (const Iterator &o) const
				{
					return _ptr == o._ptr;
				}

				template<typename T>
				bool List<T>::Iterator::operator != (const Iterator &o) const
				{
					return _ptr != o._ptr;
				}

				template<typename T>
				const T &List<T>::Iterator::operator *() const
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					return _ptr->elem;
				}

				template<typename T>
				const T *List<T>::Iterator::operator -> () const
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					return &_ptr->elem;
				}

				template<typename T>
				T &List<T>::Iterator::operator * ()
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					return _ptr->elem;
				}

				template<typename T>
				T *List<T>::Iterator::operator -> ()
				{
					if(_ptr == nullptr)
						panic("Memory::List<T>::Iterator null");

					return &_ptr->elem;
				}

				template<typename T>
				typename List<T>::_element *List<T>::Iterator::ptr()
				{
					return _ptr;
				}
			}
		}
	}

#endif
