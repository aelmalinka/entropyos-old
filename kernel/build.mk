kernel_DIR = $(SRCDIR)kernel

kernel_CFLAGS = -fPIC $(CFLAGS) $(kernel_INCLUDES) $(libk_INCLUDES)
kernel_CXXFLAGS = -fPIC $(CXXFLAGS) $(kernel_INCLUDES) $(libk_INCLUDES)

ifeq ($(ARCH),x86_64)
	kernel_CFLAGS += -mno-red-zone -mno-mmx -mno-sse -mno-sse2
	kernel_CXXFLAGS += -mno-red-zone -mno-mmx -mno-sse -mno-sse2
endif

kernel_CC_SRCS = \
	$(kernel_DIR)/main.cc \
	$(kernel_DIR)/stdlib.cc \
	$(kernel_DIR)/Memory/Pager.cc \
	$(kernel_DIR)/Memory/Allocator.cc \
	$(kernel_DIR)/Memory/new.cc \
	$(kernel_DIR)/Memory/debug_memory.cc \
	$(kernel_DIR)/Arch/X86/CPU.cc \
	$(kernel_DIR)/Arch/X86/GDT.cc \
	$(kernel_DIR)/Arch/X86/Driver/Serial.cc \
	$(kernel_DIR)/Arch/X86/AMD/Bobcat.cc \
	$(kernel_DIR)/Arch/X86/AMD/Vishera.cc

kernel_CC_OBJS = $(foreach src,$(kernel_CC_SRCS),$(patsubst %.cc,$(BUILD)/%.cc.o,$(src)))
kernel_CC_DEPS = $(foreach src,$(kernel_CC_SRCS),$(patsubst %.cc,$(BUILD)/%.cc.dep,$(src)))

kernel_OBJS := $(kernel_CC_OBJS)
kernel_DEPS := $(kernel_CC_DEPS)

kernel: $(BUILD)/libkernel.a

$(BUILD)/libkernel.a: $(kernel_OBJS)
	$(AR) rcs $@ $^

clean-kernel:
	$(RM) $(wildcard $(BUILD)/libkernel.a $(kernel_OBJS))

$(eval $(call CXX-rule,$(kernel_DIR),$$(kernel_CXXFLAGS),$(kernel_DIR)))

.PHONY: kernel clean-kernel

-include $(kernel_DEPS)
