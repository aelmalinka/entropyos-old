EFI_DIR = $(SRCDIR)EFI

EFI_ARCH := $(shell uname -m | sed s,i[3456789]86,ia32,)
EFI_PATH := /usr/lib
EFI_INCLUDE := /usr/include/efi
EFI_INCLUDES := -I$(EFI_INCLUDE) -I$(EFI_INCLUDE)/$(EFI_ARCH) -I$(EFI_INCLUDE)/protocol $(libk_INCLUDES)

EFI_LIBS := -lefi -lgnuefi -L$(BUILD) -lk -lkernel -lk
EFI_CRT0_OBJ := $(EFI_PATH)/crt0-efi-$(ARCH).o
EFI_LDS := /usr/lib/elf_$(EFI_ARCH)_efi.lds

EFI_CFLAGS := -fno-stack-protector -fno-strict-aliasing -fPIC -DGNU_EFI_USE_MS_ABI $(CFLAGS) $(libk_C_INCLUDES) -Wno-pedantic $(EFI_INCLUDES)
EFI_CXXFLAGS := -fno-stack-protector -fno-strict-aliasing -fPIC $(CXXFLAGS) $(libk_CXX_INCLUDES) $(EFI_INCLUDES) -Wno-old-style-cast -Wno-useless-cast
EFI_LDFLAGS = -nostdlib -znocombreloc -T $(EFI_LDS) -shared -Bsymbolic -L$(EFI_PATH)

EFI_LDFLAGS += -zdefs -no-allow-shlib-undefined

ifeq ($(ARCH),x86_64)
	EFI_CFLAGS += -mno-red-zone -mno-mmx -mno-sse -mno-sse2
	EFI_CXXFLAGS += -mno-red-zone -mno-mmx -mno-sse -mno-sse2
endif

EFI_C_SRCS = \
	$(EFI_DIR)/main.c \
	$(EFI_DIR)/util.c
EFI_CC_SRCS = \
	$(EFI_DIR)/main.cc \
	$(EFI_DIR)/Boot.cc \
	$(EFI_DIR)/System.cc

EFI_C_OBJS = $(foreach src,$(EFI_C_SRCS),$(patsubst %.c,$(BUILD)/%.c.o,$(src)))
EFI_C_DEPS = $(foreach src,$(EFI_C_SRCS),$(patsubst %.c,$(BUILD)/%.c.dep,$(src)))

EFI_CC_OBJS = $(foreach src,$(EFI_CC_SRCS),$(patsubst %.cc,$(BUILD)/%.cc.o,$(src)))
EFI_CC_DEPS = $(foreach src,$(EFI_CC_SRCS),$(patsubst %.cc,$(BUILD)/%.cc.dep,$(src)))

EFI: $(BUILD)/kernel.efi $(BUILD)/kernel.sym

$(BUILD)/kernel.efi.so: $(EFI_C_OBJS) $(EFI_CC_OBJS) $(EFI_LDS) $(EFI_CRT0_OBJ) $(EFI_CRTI_OBJ) $(EFI_CRTN_OBJ) $(BUILD)/libkernel.a $(BUILD)/libk.a $(EFI_DIR)/build.mk
	$(LD) $(EFI_LDFLAGS) $(EFI_CRT0_OBJ) $(EFI_C_OBJS) $(EFI_CC_OBJS) -o $@ $(EFI_LIBS)

$(BUILD)/kernel.sym: $(BUILD)/kernel.efi.so
	$(OBJCOPY) --only-keep-debug $< $@

$(BUILD)/kernel.efi: $(BUILD)/kernel.efi.so
	$(OBJCOPY) -j .text -j .sdata -j .data -j .dynamic -j .dynsym -j .rel -j .rela -j .reloc --target=efi-app-$(EFI_ARCH) $< $@
	$(STRIP) $@

$(BUILD)/$(EFI_DIR)/%.o: $(EFI_DIR)/%.s $(EFI_DIR)/build.mk $(SRCDIR)Makefile
	$(DIR_GUARD)
	$(AS) -o $@ $<

$(eval $(call CC-rule,$(EFI_DIR),$$(EFI_CFLAGS),$(EFI_DIR)))
$(eval $(call CXX-rule,$(EFI_DIR),$$(EFI_CXXFLAGS),$(EFI_DIR)))

clean-EFI:
	$(RM) $(wildcard $(EFI_C_OBJS) $(EFI_CC_OBJS) $(BUILD)/kernel.efi $(BUILD)/kernel.sym $(BUILD)/kernel.efi.so $(EFI_CRTI_OBJ) $(EFI_CRTN_OBJ))

.PHONY: clean-EFI EFI

include $(EFI_C_DEPS) $(EFI_CC_DEPS)
