/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "stdlib.h"
#include <cstring>
#include <cctype>
#include <print.hh>

using namespace std;
using namespace Entropy::Eos;

void abort()
{
	panic("Abort called");
}

long int strtol(const char * __restrict s, char ** __restrict end, int base)
{
	int inv = 1;
	long ret = 0l;

	while(isspace(*s)) ++s;

	if(*s == '+' || *s == '-') {
		if(*s == '-') {
			inv = -1;
		}
		++s;
	}

	if(*s == '0') {
		if(tolower(*++s) == 'x') {
			if(base == 16 || base == 0) {
				base = 16;
				++s;
			} else {
				if(end != nullptr)
					*end = const_cast<char *>(reinterpret_cast<const char *>(s));

				return 0;
			}
		} else if(base == 8 || base == 0) {
			base = 8;
		}
	}

	if(base == 0)
		base = 10;

	const char *b = s - 1;
	int x = 0u;
	while(*s) {
		if(!isalnum(*s)) {
			break;
		}

		++s;
	}

	--s;
	while(s != b) {
		auto digit = 0;
		if(isalpha(*s)) {
			digit = tolower(*s) - 'a' + 10;
		} else {
			digit = *s - '0';
		}

#		ifdef DEBUG
			print("[strtol]: digit: ",digit," x: ",x,"\n");
#		endif
		if(digit < base) {
			auto y = x++;
			auto raise = 1l;
			if(y > 0) while(y--) raise *= base;
			ret += raise * digit;
		} else {
#			ifdef DEBUG
				print("[strtol]: digit is not part of number\n");
#			endif
			ret = 0; x = 0;
		}

		--s;
	}

	if(end != nullptr)
		*end = const_cast<char *>(reinterpret_cast<const char *>(s));

	return ret * inv;
}

void *realloc(void *where, size_t sz)
{
	unsigned char *ret = reinterpret_cast<unsigned char *>(malloc(sz));

	if(where != nullptr) {
		memcpy(ret, where, sz);
		free(where);
	}

	return ret;
}
