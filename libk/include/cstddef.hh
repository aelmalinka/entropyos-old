/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_CSTDDEF_INC
#	define ENTROPY_EOS_LIBK_CSTDDEF_INC

#	include <stddef.h>

	namespace std
	{
		using ::ptrdiff_t;
		using ::size_t;

		using nullptr_t = decltype(nullptr);
	}

#endif
