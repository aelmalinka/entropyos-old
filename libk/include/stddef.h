/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_LIBK_STDDEF_INC
#	define ENTROPY_EOS_LIBK_STDDEF_INC

	typedef __PTRDIFF_TYPE__ ptrdiff_t;
	typedef __SIZE_TYPE__ size_t;

#	ifdef __cplusplus
#		define NULL 0
#	else
#		define NULL ((void *)0)
#	endif

#	define offsetof(TYPE, MEMBER) __builtin_offsetof(TYPE, MEMBER)

#	ifdef __cplusplus
#		include <cstddef>
#	endif

#endif
