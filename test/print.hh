/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_TEST_PRINT_INC
#	define ENTROPY_EOS_TEST_PRINT_INC

#	include "../libk/print.hh"

#endif
