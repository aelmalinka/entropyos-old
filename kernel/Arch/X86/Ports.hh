/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined ENTROPY_EOS_X86_PORTS_INC
#	define ENTROPY_EOS_X86_PORTS_INC

#	include <cstdint>

	namespace Entropy
	{
		namespace Eos
		{
			namespace X86
			{
				static inline void writeb(const std::uint16_t, const std::uint8_t);
				static inline void writew(const std::uint16_t, const std::uint16_t);
				static inline void writel(const std::uint16_t, const std::uint32_t);

				static inline std::uint8_t readb(const std::uint16_t);
				static inline std::uint16_t readw(const std::uint16_t);
				static inline std::uint32_t readl(const std::uint16_t);
			}
		}
	}

#	include "Ports.impl.hh"

#endif
