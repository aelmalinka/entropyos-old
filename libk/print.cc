/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "print.hh"

namespace Entropy
{
	namespace Eos
	{
		[[noreturn]] void panic(const char *s)
		{
			print(s);
			while(true);
		}

		void print()
		{}

		namespace detail
		{
			Output *_out = nullptr;

			void _print(const char *s)
			{
				if(_out != nullptr)
					_out->Write(s);
			}

			void _print(const std::intmax_t v)
			{
				if(_out != nullptr)
					_out->Write(v);
			}

			void _print(void *p)
			{
				if(_out != nullptr)
					_out->Write(p);
			}
		}
	}
}
