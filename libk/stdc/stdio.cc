/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "stdio.h"
#include <print.hh>
#include <cstring>

using namespace Entropy::Eos;
using namespace std;

FILE _stdin = 0;
FILE _stdout = 1;
FILE _stderr = 2;

FILE *stdin = &_stdin;
FILE *stdout = &_stdout;
FILE *stderr = &_stderr;

size_t fread(void *, size_t, size_t, FILE *)
{
	print("[fread]: to be implemented\n");

	return 0;
}

size_t fwrite(const void *data, size_t sz, size_t num, FILE *file)
{
	if(file == stdout || file == stderr) {
		if(sz == sizeof(char) && num == strlen(reinterpret_cast<const char *>(data))) {
			print(reinterpret_cast<const char *>(data));

			return num;
		}
	}

	print("[fwrite]: only partially implemented, ", sz, " ", sizeof(char), ", ", num, " ", strlen(reinterpret_cast<const char *>(data)),"\n");
	return 0;
}

int printf(const char *, ...)
{
	print("[printf]: to be implemented\n");

	return 0;
}

int fprintf(FILE *, const char *, ...)
{
	print("[fprintf]: to be implemented\n");

	return 0;
}

int snprintf(char * __restrict, size_t, const char * __restrict, ...)
{
	print("[snprintf]: to be implemented\n");

	return 0;
}

int asprintf(char **, const char *, ...)
{
	print("[asprintf]: to be implemented\n");

	return 0;
}

int vprintf(const char *, va_list)
{
	print("[vprintf]: to be implemented\n");

	return 0;
}

int vfprintf(FILE *, const char *, va_list)
{
	print("[vfprintf]: to be implemented\n");

	return 0;
}

int vsnprintf(char * __restrict, size_t, const char * __restrict, va_list)
{
	printf("[vsnprintf]: to be implemented\n");

	return 0;
}

int vasprintf(char **, const char *, va_list)
{
	printf("[vasprintf]: to be implemented\n");

	return 0;
}
