/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <cstdlib>
#include <cstring>
#include <print.hh>
#include "Memory/new.hh"

using namespace std;
using namespace Entropy::Eos;
using namespace Entropy::Eos::Memory;

extern "C" void *malloc(size_t sz)
{
	if(_alloc == nullptr) {
		panic("malloc w/o allocator");
	}

	return _alloc->allocate(sz);
}

extern "C" void free(void *ptr)
{
	if(_alloc == nullptr) {
		panic("free w/o allocator");
	}

	_alloc->deallocate(ptr);
}

// 2018-01-21 AMR TODO: why on earth can't I call malloc from here
void *calloc(size_t elem, size_t sz)
{
	if(_alloc == nullptr) {
		panic("calloc w/o allocator");
	}

	unsigned char *ret = reinterpret_cast<unsigned char *>(_alloc->allocate(elem * sz));
	if(ret == nullptr) return nullptr;

	memset(ret, 0, elem * sz);

	return ret;
}
