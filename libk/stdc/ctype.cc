/*	Copyright 2018 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "ctype.h"
// 2018-01-22 AMR TODO: proper unicode

int isalnum(int c)
{
	return isalpha(c) || isdigit(c);
}

int isalpha(int c)
{
	return isupper(c) || islower(c);
}

int isdigit(int c)
{
	return c >= 48 && c <= 57;
}

int islower(int c)
{
	return c >= 97 && c <= 122;
}

int isspace(int c)
{
	return
		c == ' ' ||
		c == '\f' ||
		c == '\n' ||
		c == '\r' ||
		c == '\t' ||
		c == '\v'
	;
}

int isupper(int c)
{
	return c >= 65 && c <= 90;
}

int tolower(int c)
{
	if(isalpha(c) && isupper(c)) {
		return c + 32;
	} else {
		return c;
	}
}

int toupper(int c)
{
	if(isalpha(c) && islower(c)) {
		return c - 32;
	} else {
		return c;
	}
}
