/*	Copyright 2015 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "PageTableEntry.hh"
#include <print.hh>

using namespace Entropy::Eos::Memory;
using namespace std;

using Size = PageTableEntry::Size;
using Type = PageTableEntry::Type;

PageTableEntry::PageTableEntry(const uint64_t value, Size size, Type type)
	: _value(value), _size(size), _type(type)
{
	// 2015-11-29 AMR TODO: sanity check
}

PageTableEntry::PageTableEntry(void *where, Size ps, Type which, bool Writable, bool User, bool Executable, bool Writethrough, bool CacheDisabled, bool Global)
	: _value(0), _size(ps), _type(which)
{
	// 2015-11-11 AMR TODO: PAT needs to be something
	_value = 0x1;	// 2015-11-11 AMR NOTE: only present is enabled
	_value |= (Writable ? 0x2 : 0);
	_value |= (User ? 0x4 : 0);
	_value |= (!Executable ? 0x8000'0000'0000'0000 : 0);
	_value |= (Writethrough ? 0x8 : 0);
	_value |= (CacheDisabled ? 0x10 : 0);
	_value |= (Global ? 0x100 : 0x0);

	switch(ps)
	{
		case Size::FourKb:
			_value |= reinterpret_cast<uint64_t>(where) & 0x000F'FFFF'FFFF'F000;
		break;
		case Size::TwoMb:
			switch(which)
			{
				case Type::PageMapLevel4:
				case Type::PageDirectoryPointer:
					_value |= reinterpret_cast<uint64_t>(where) & 0x000F'FFFF'FFFF'F000;
				break;
				case Type::PageDirectory:
					_value |= reinterpret_cast<uint64_t>(where) & 0x000F'FFFF'FFFE'0000;
					_value |= 0x80;	// 2015-11-11 AMR NOTE: enable page size;
				break;
				default:
					panic("PageTableEntry has an invalid PageMap Level selected, TwoMb");
			}
		break;
		case Size::OneGb:
			switch(which)
			{
				case Type::PageMapLevel4:
					_value |= reinterpret_cast<uint64_t>(where) & 0x000F'FFFF'FFFF'F000;
				break;
				case Type::PageDirectoryPointer:
					_value |= reinterpret_cast<uint64_t>(where) & 0x000F'FFFF'C000'0000;
					_value |= 0x80;	// 2015-11-11 AMR NOTE: enable page size;
				break;
				default:
					panic("PageTableEntry has an invalid PageMap Level selected, OneGb");
			}
		break;
		default:
			panic("PageTableEntry has an invalid size selected");
	}
}

PageTableEntry::~PageTableEntry() = default;

const uint64_t &PageTableEntry::Value() const
{
	return _value;
}

const void *PageTableEntry::Where() const
{
	switch(_size)
	{
		case Size::FourKb:
			return reinterpret_cast<const void *>(_value & 0x000F'FFFF'FFFF'F000);
		break;
		case Size::TwoMb:
			switch(_type)
			{
				case Type::PageMapLevel4:
				case Type::PageDirectoryPointer:
					return reinterpret_cast<const void *>(_value & 0x000F'FFFF'FFFF'F000);
				break;
				case Type::PageDirectory:
					return reinterpret_cast<const void *>(_value & 0x000F'FFF'FFFE'0000);
				break;
				default:
					panic("PageTableEntry has an invalid PageMap Level, TwoMb");
					return nullptr;
			}
		break;
		case Size::OneGb:
			switch(_type)
			{
				case Type::PageMapLevel4:
					return reinterpret_cast<const void *>(_value & 0x000F'FFFF'FFFF'F000);
				break;
				case Type::PageDirectoryPointer:
					return reinterpret_cast<const void *>(_value & 0x000F'FFFF'C000'000);
				break;
				default:
					panic("PageTableEntry has an invalid PageMap Level, OneGb");
					return nullptr;
			}
		break;
		default:
			panic("PageTableEntry has an invalid size");
			return nullptr;
	}
}

const Size &PageTableEntry::size() const
{
	return _size;
}

const Type &PageTableEntry::type() const
{
	return _type;
}

bool PageTableEntry::Writable() const
{
	return _value & 0x2;
}

bool PageTableEntry::User() const
{
	return _value & 0x4;
}

bool PageTableEntry::Executable() const
{
	return !(_value & 0x8000'0000'0000'0000);
}

bool PageTableEntry::Writethrough() const
{
	return _value & 0x8;
}

bool PageTableEntry::CacheDisabled() const
{
	return _value & 0x10;
}

bool PageTableEntry::Global() const
{
	return _value & 0x100;
}
